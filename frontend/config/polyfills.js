// babel
require('babel-polyfill');

// URLSearchParams
require('url-search-params-polyfill');

// fetch() polyfill for making API calls.
require('whatwg-fetch');

