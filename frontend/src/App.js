import { connect } from 'react-redux'

import {updateEditor, compile, receiveLoad, fetchArtifact} from './logic/actions';

import AppView from './AppView';
//import {updateSelectedVersion} from "./logic/actions";

const mapStateToProps = (state) => {
  return {
    yaccValue: state.editors.yacc,
    lexValue: state.editors.lex,
    clearValue: state.editors.clear,
    regexValue: state.editors.regex,

    console: state.compiler.console,
    notice: state.compiler.notice,

    yaccVersions: state.versions.yacc,
    yaccSelectedVersion: state.versions.yaccSelected,
    yaccLabel: state.versions.yaccLabel,

    lexVersions: state.versions.lex,
    lexSelectedVersion: state.versions.lexSelected,
    lexLabel: state.versions.lexLabel,

    clearVersions: state.versions.clear,
    clearSelectedVersion: state.versions.clearSelected,
    clearLabel: state.versions.clearLabel,

    regexVersions: state.versions.regex,

    expressions: state.versions.regex,

    progress: state.compiler.inProgress || state.versions.createInProgress > 0
      || state.versions.saveInProgress > 0
      || state.versions.loadInProgress > 0 || state.versions.getInProgress > 0,

    regexOutput: state.regex.explanation,
    regexTree: state.regex.tree,

    autosaveVersions: state.autosave.versions,

    artifactText: state.compiler.artifact,
    artifactList: state.compiler.artifacts
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeYacc: (val) => {
      dispatch(updateEditor('yacc', val));
    },
    onChangeLex: (val) => {
      dispatch(updateEditor('lex', val));
    },
    onChangeClear: (val) => {
      dispatch(updateEditor('clear', val));
    },
    onChangeRegex: (val) => {
      dispatch(updateEditor('regex', val.target.value));
    },
    onChooseRegex: (val) => {
      dispatch(updateEditor('regex', val));
    },
    onCompileYaccLex: () => {
      dispatch(compile(['clear_compiler']));
    },
    onCompileClear: () => {
      dispatch(compile(['explainer']));
    },
    onCompileRegex: () => {
      dispatch(compile(['regex_text', 'tree_text']));
    },
    onLoadArtifact: (artifact) => {
      dispatch(fetchArtifact(artifact));
    },
    onChangeYaccVersion: (val) => {
      // start zapytania API
      //dispatch(receiveLoad('yacc', val, 'YACC Source ' + val));
    },
    onChangeLexVersion: (val) => {
      //dispatch(updateSelectedVersion('lex', val));
      //dispatch(receiveLoad('lex', val, 'LEX Source ' + val));
    }
  };
};

const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppView);

export default App;
