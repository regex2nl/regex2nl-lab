import React, { Component } from 'react';

import WrappedAceEditor from './components/WrappedAceEditor';
import GridBlock from './components/GridBlock';
import YaccGridBlock from './components/YaccGridBlock';
import GridBlockContent from './components/GridBlockContent';
import Console from './components/Console';
import SmartSnackbar from './components/SmartSnackbar';
import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';
import ClearGridBlock from './components/ClearGridBlock';
import RegexGridBlock from './components/RegexGridBlock';
import OutputTextArea from './components/OutputTextArea';
import {List, ListItem} from 'material-ui/List';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import Drawer from 'material-ui/Drawer';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Snackbar from 'material-ui/Snackbar';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Paper from 'material-ui/Paper';

import 'normalize.css/normalize.css';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import './App.css';


import brace from 'brace';
import 'brace/theme/chrome';
import 'brace/mode/c_cpp';
import '../syntax-color/mode-lex';
import '../syntax-color/mode-yacc';
import '../syntax-color/mode-clear';
import '../syntax-color/syntax-color.css';

import {Responsive, WidthProvider} from 'react-grid-layout';
import {SelectField} from "material-ui";
const ResponsiveReactGridLayout = WidthProvider(Responsive);

class AppView extends Component {

  constructor(props) {
    super(props);

    this.state = {consoleOpen: false, menuOpen:false, autosaveOpen: false, autosaveOption: '0',
    layout: AppView.layouts, view: 'grid'};

    this.onConsoleRequestChange = this.onConsoleRequestChange.bind(this);
    this.onChangeClearVersion = this.onChangeClearVersion.bind(this);
    this.onChangeYaccVersion = this.onChangeYaccVersion.bind(this);
    this.onChangeLexVersion = this.onChangeLexVersion.bind(this);
    this.onCompileYacc = this.onCompileYacc.bind(this);
    this.onCompileLex = this.onCompileLex.bind(this);
    this.onCompileClear = this.onCompileClear.bind(this);
    this.onCompileRegex = this.onCompileRegex.bind(this);
    this.onCreateYacc = this.onCreateYacc.bind(this);
    this.onCreateLex = this.onCreateLex.bind(this);
    this.onCreateClear = this.onCreateClear.bind(this);
    this.onAutoSaveAction = this.onAutoSaveAction.bind(this);
    this.onAutoSaveChange = this.onAutoSaveChange.bind(this);
    this.onCreateRegex = this.onCreateRegex.bind(this);
    this.onMenuRequestChange = this.onMenuRequestChange.bind(this);
    this.onResetLayuotRequest = this.onResetLayuotRequest.bind(this);
    this.onLayoutChange = this.onLayoutChange.bind(this);
    this.onChangeView = this.onChangeView.bind(this);
    this.onToggleFullscreen = this.onToggleFullscreen.bind(this);
  }

  onChangeClearVersion(val, loadLexYacc) {
    this.props.api.loadVersion('clear', val, loadLexYacc);
  }

  onChangeYaccVersion(val) {
    this.props.api.loadVersion('yacc', val);
  }

  onChangeLexVersion(val) {
    this.props.api.loadVersion('lex', val);
  }

  onCompileYacc() {
    this.props.api.saveYaccLex('yacc');
  }

  onCompileLex() {
    this.props.api.saveYaccLex('lex');
  }

  onCreateClear() {
    this.props.api.createClear();
  }

  onCreateYacc() {
    this.props.api.createYaccLex('yacc');
  }

  onCreateLex() {
    this.props.api.createYaccLex('lex');
  }

  onCreateRegex(name) {
    this.props.api.saveRegex(name);
  }

  onCompileClear() {
    this.props.api.saveClear(this.props.clearSelectedVersion, this.props.clearValue);
  }

  onCompileRegex() {
    this.props.api.compileRegex();
  }

  onConsoleRequestChange(state) {
    this.setState({consoleOpen: state});
  }

  onMenuRequestChange(state) {
    this.setState({menuOpen: state});
  }

  onResetLayuotRequest() {
    this.setState({layout: AppView.layouts});
  }

  onLayoutChange(layout, layouts) {
    this.setState({layout:layouts});
  }

  onAutoSaveAction(action) {
    if (action == 'open') {
      this.setState({autosaveOpen: true});
    } else if (action == 'close') {
      this.setState({autosaveOpen: false});
    } else if (action == 'submit') {
      this.props.autosaveManager.load(this.state.autosaveOption);
      this.setState({autosaveOpen: false});
    }
  }

  onAutoSaveChange(event, key, value) {
    this.setState({autosaveOption: value});
  }

  onChangeView(view) {
    this.setState({view: view});
  }

  onToggleFullscreen(view) {
    if (this.state.view == 'artifact-viewer' || this.state.view == 'grid') {
      this.onChangeView(view);
    } else {
      this.onChangeView('grid');
    }
  }

  renderView(view) {
    switch (view) {
      case 'artifact-viewer':
        return this.renderArtifactViewer();
        break;

      case 'Yacc':
      case 'Lex':
      case 'Clear':
      case 'Regex':
        return this.renderSingle(view);
        break;

      default:
        return this.renderGrid();
    }
  }

  renderSingle(type) {
    let elem = this['render' + type]();

    let height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    height = parseInt(height) - 64;

    return React.cloneElement(elem, { style: { height: height + 'px' } });
  }

  renderYacc() {
    return (
      <YaccGridBlock title="Yacc" key="yacc"
                     version={this.props.yaccSelectedVersion}
                     versionLabel={this.props.yaccLabel}
                     availableVersions={this.props.yaccVersions}
                     onCreateVersion={this.onCreateYacc}
                     onChange={this.onChangeYaccVersion}
                     onFileLoad={this.props.onChangeYacc}
                     onCompile={this.onCompileYacc}
                     onFullscreen={() => this.onToggleFullscreen('Yacc')}>
        <GridBlockContent>
          <WrappedAceEditor name="yacc-editor" mode="yacc" value={this.props.yaccValue} onChange={this.props.onChangeYacc} />
        </GridBlockContent>
      </YaccGridBlock>
    );
  }

  renderLex() {
    return (
      <YaccGridBlock title="Lex"
                     key="lex"
                     version={this.props.lexSelectedVersion}
                     versionLabel={this.props.lexLabel}
                     availableVersions={this.props.lexVersions}
                     onCreateVersion={this.onCreateLex}
                     onChange={this.onChangeLexVersion}
                     onFileLoad={this.props.onChangeLex}
                     onCompile={this.onCompileLex}
                     onFullscreen={() => this.onToggleFullscreen('Lex')}>
        <GridBlockContent>
          <WrappedAceEditor name="lex-editor" mode="lex" value={this.props.lexValue} onChange={this.props.onChangeLex} />
        </GridBlockContent>
      </YaccGridBlock>
    );
  }

  renderClear() {
    return (
      <ClearGridBlock title="Clear"
                      key="clear"
                      version={this.props.clearSelectedVersion}
                      versionLabel={this.props.clearLabel}
                      availableVersions={this.props.clearVersions}
                      onCreateVersion={this.onCreateClear}
                      onChange={this.onChangeClearVersion}
                      onFileLoad={this.props.onChangeClear}
                      onCompile={this.onCompileClear}
                      onFullscreen={() => this.onToggleFullscreen('Clear')}>
        <GridBlockContent>
          <WrappedAceEditor name="clear-editor" value={this.props.clearValue} mode="clear" onChange={this.props.onChangeClear} />
        </GridBlockContent>
      </ClearGridBlock>
    );
  }

  renderRegex() {
    return (
      <RegexGridBlock title="Regex"
                      key="regex"
                      onCompile={this.onCompileRegex}
                      expressions={this.props.expressions}
                      onChooseRegex={this.props.onChooseRegex}
                      availableVersions={this.props.regexVersions}
                      onCreateRegex={this.onCreateRegex}
                      onFullscreen={() => this.onToggleFullscreen('Regex')}>
        <GridBlockContent paddingH={10}>
          <TextField hintText="Regular expression" fullWidth={true}
                     value={this.props.regexValue} onChange={this.props.onChangeRegex} />
          <textarea className="textarea" placeholder = "Explanation" disabled value={this.props.regexOutput}/>
          <OutputTextArea className="textarea" regexOutput={this.props.regexTree}/>
        </GridBlockContent>
      </RegexGridBlock>
    );
  }

  renderGrid() {
    return (
      <ResponsiveReactGridLayout layouts={this.state.layout} draggableHandle=".gridHandle" onLayoutChange={this.onLayoutChange}>
        {this.renderYacc()}
        {this.renderLex()}
        {this.renderClear()}
        {this.renderRegex()}
      </ResponsiveReactGridLayout>
    );
  }

  renderArtifactViewer() {
    if (this.props.artifactList.length <= 0) {
      return (
        <Card>
          <CardHeader title="Information"></CardHeader>
          <CardText>
            Compilation process needs to be started in order to view generated files
          </CardText>
        </Card>
      );
    }

    let items = [];

    for (let k in this.props.artifactList) {
      if (this.props.artifactList.hasOwnProperty(k))
        items.push(
          <ListItem key={k} leftAvatar={<FontIcon className="material-icons">folder_shared</FontIcon>}
                    primaryText={this.props.artifactList[k].name}
                    secondaryText={this.props.artifactList[k].env}
                    onTouchTap={() => {this.props.onLoadArtifact(this.props.artifactList[k])}} />
        );
    }

    return (
      <div className="artifact-viewer">
        <div className="left">
          <Paper>
            <List>
              {items}
            </List>
          </Paper>
        </div>
        <div className="right">
          <Paper>
            <textarea className="artifact-viewer-textarea" readOnly={true} value={this.props.artifactText} />
          </Paper>
        </div>
      </div>
    );
  }

  render() {
    let items = [];

    for (let k in this.props.autosaveVersions) {
      if (this.props.autosaveVersions.hasOwnProperty(k))
        items.push(<MenuItem key={k} value={k} primaryText={this.props.autosaveVersions[k]} />);
    }

    const autosaveOptions = [
      <FlatButton
        label="Cancel"
        primary={false}
        onTouchTap={() => this.onAutoSaveAction('close')}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => this.onAutoSaveAction('submit')}
      />,
    ];

    //<SelectField>{items}</SelectField>

    return (
      <div className="App">
        <AppBar title={"Regex2NL"} onRightIconButtonTouchTap={() => this.onConsoleRequestChange(true)}
                onLeftIconButtonTouchTap={() => {this.onMenuRequestChange(true);/*this.onAutoSaveAction('open');*/}}
                iconElementRight={
                    <FlatButton label="Console"></FlatButton>
                }
        />
        <Dialog modal={true} actions={autosaveOptions} open={this.state.autosaveOpen} title={"Load autosaved data"}>
          <SelectField value={this.state.autosaveOption} onChange={this.onAutoSaveChange}>{items}</SelectField>
        </Dialog>
        <Dialog
            title="In progress"
            modal={true}
            open={this.props.progress}
         ><CircularProgress size={60} thickness={7} /></Dialog>
        <Drawer open={this.state.consoleOpen} docked={false} openSecondary={true} width={400}
                onRequestChange={this.onConsoleRequestChange}>
          <Console value={this.props.console} />
        </Drawer>

        <Drawer open={this.state.menuOpen} docked={false} openSecondary={false} width={400}
                onRequestChange={this.onMenuRequestChange}>
          <MenuItem onTouchTap={() => {this.onAutoSaveAction('open'); this.onMenuRequestChange(false);}}>Autosave versions</MenuItem>
          <MenuItem onTouchTap={() => {this.onResetLayuotRequest(); this.onMenuRequestChange(false);}}>Reset layout</MenuItem>
          <MenuItem onTouchTap={() => {this.onChangeView('grid'); this.onMenuRequestChange(false);}}>Compilation view</MenuItem>
          <MenuItem onTouchTap={() => {this.onChangeView('artifact-viewer'); this.onMenuRequestChange(false);}}>Output view</MenuItem>
        </Drawer>

        <SmartSnackbar message={this.props.notice} />
        {this.renderView(this.state.view)}
      </div>
    );
  }
}

AppView.layouts = {
  lg: [
    { i: 'yacc', x: 0, y: 0, w: 6, h: 4, minW: 2 },
    { i: 'lex', x: 6, y: 3, w: 3, h: 2, minW: 2 },
    { i: 'clear', x: 6, y: 0, w: 3, h: 2, minW: 2 },
    { i: 'regex', x: 9, y: 0, w: 3, h: 4, minH: 2, minW: 2 },
  ]
};

export default AppView;
