import {startVersionsGet, failVersionsSave, failVersionsGet, failVersionsLoad, startVersionsCreate,
  receiveVersionsCreate, failVersionsCreate
} from "../logic/actions";
import {receiveVersionsGet} from "../logic/actions";
import {startVersionsLoad} from "../logic/actions";
import {receiveVersionsLoad} from "../logic/actions";
import {startVersionsSave, compile} from "../logic/actions";
import {receiveVersionsSave} from "../logic/actions";
import Deferred from "../utils/Deferred";

class ApiConnection {

  constructor(store, baseUrl) {
    this.store = store;
    this.baseUrl = baseUrl;

    // what are we expecting compiler to provide
    this.compilerExpected = null;

    // promise which will be rejected/fulfilled after compiler
    this.compilerPromise = null;

    // binding state from Redux
    this.clearCompiler = null;
    this.explainer = null;
    this.yacc = null;
    this.lex = null;
    this.clear = null;
    this.language = 'pl';
    this.yaccSource = '';
    this.lexSource = '';
    this.clearSource = '';
    this.regexSource = '';
    this.yaccDirty = true;
    this.lexDirty = true;

    this.store.subscribe(this.onChange.bind(this));
  }

  fetchApi(api, method = 'GET', parameters) {
    let url = this.baseUrl + api;

    // create URLSearchParams from javascript object
    let params = new URLSearchParams();

    if (parameters != undefined) {
      for (let p in parameters) {
        if (parameters.hasOwnProperty(p)) {
          params.append(p, parameters[p]);
        }
      }
    }

    // depending on request method add params as query string or as body
    let p;

    if (method == 'GET') {
      let qs = params.toString();

      p = fetch(url + (qs ? '?'+qs : ''), {
        credentials: 'include'
      });
    } else {
      p = fetch(url, {
        method: method,
        body: params,
        credentials: 'include'
      });
    }

    // consider status codes other than <200;300) failure
    return p.then(function (response) {
      if (response.status >= 200 && response.status < 300) {
        return response;
      } else {
        let error = new Error(response.statusText);
        throw error;
      }
    });
  }

  onChange() {
    let newState = this.store.getState();

    this.clearCompiler = newState.versions.clearCompiler;
    this.explainer = newState.versions.explainer;
    this.yacc = newState.versions.yaccSelected;
    this.lex = newState.versions.lexSelected;
    this.clear = newState.versions.clearSelected;
    this.language = newState.versions.clearLanguage;
    this.yaccSource = newState.editors.yacc;
    this.lexSource = newState.editors.lex;
    this.clearSource = newState.editors.clear;
    this.regexSource = newState.editors.regex;
    this.yaccDirty = newState.versions.yaccDirty;
    this.lexDirty = newState.versions.lexDirty;

    // are we waiting for compiler?
    if (this.compilerExpected === null)
      return;

    // if compilation finished, fulfill or reject promise
    if (!newState.compiler.inProgress) {
      if (newState.compiler.output === null || !(this.compilerExpected in newState.compiler.output)) {
        this.compilerExpected = null;
        this.compilerPromise.reject();

        return;
      }

      let out = newState.compiler.output[this.compilerExpected];
      this.compilerExpected = null;
      this.compilerPromise.resolve(out);
    }
  }

  compile(what) {
    this.compilerExpected = what;
    this.compilerPromise = new Deferred();

    this.store.dispatch(compile([what]));

    return this.compilerPromise.promise;
  }

  getVersions(type, loadFirst = false) {
    this.store.dispatch(startVersionsGet(type));

    let p = this.fetchApi(type).then(function (response) {
      return response.json();
    }).then((value) => {
      this.store.dispatch(receiveVersionsGet(type, value));

      if (loadFirst && type == 'clear') {
        this.loadVersion('clear', value[0].id, true);
      }
    }).catch((e) => {
      this.store.dispatch(failVersionsGet(type, e.message));
    });
  }

  loadVersion(type, id, full = false) {
    this.store.dispatch(startVersionsLoad(type));

    return this.fetchApi(type + '/' + id).then(function (response) {
      return response.json();
    }).then((value) => {
      let dependencies = null;
      let language = null;

      if (type == 'clear') {
        dependencies = { yacc: value.yacc.id, lex: value.lex.id };
        language = value.language.alpha2;
      }

      this.store.dispatch(receiveVersionsLoad(type, value.id, value.version, language, value.source,
        value.repository, dependencies, value.clear_compiler_repository, full));

      if (full && type == 'clear') {
        this.loadVersion('yacc', dependencies.yacc);
        this.loadVersion('lex', dependencies.lex);
      }
    }).catch((e) => {
      this.store.dispatch(failVersionsLoad(type, e.message));
    });
  }

  saveYaccLex(type) {
    let id = this[type];
    let source = this[type + 'Source'];

    this.compile('clear_compiler')
      .then((repository) => this.doSaveYaccLex(type, id, source, repository));
  }

  doSaveYaccLex(type, id, source, repository) {
    this.store.dispatch(startVersionsSave(type));

    let out = this.fetchApi(type + '/' + id, 'PUT', {
        source: source
    }).then(function (response) {
      return response.json();
    }).then((response) => {
      this.store.dispatch(receiveVersionsSave(type, response.id, response.version));
      this.getVersions(type);
    }).then(function () {
      return repository;
    }).catch((e) => {
      this.store.dispatch(failVersionsSave(type, e.message));
    });

    return out;
  }

  createYaccLex(type) {
    let source = this[type + 'Source'];

    this.compile('clear_compiler')
      .then((repository) => this.doCreateYaccLex(type, source, repository));
  }

  doCreateYaccLex(type, source, repository) {
    this.store.dispatch(startVersionsCreate(type));

    let out = this.fetchApi(type, 'POST', {
      source: source
    }).then(function (response) {
      return response.json();
    }).then((response) => {
      this.store.dispatch(receiveVersionsCreate(type, response.id, response.version));
      this.getVersions(type);
    }).then(function () {
      return repository;
    }).catch((e) => {
      this.store.dispatch(failVersionsCreate(type, e.message));
    });

    return out;
  }

  saveClear() {
    let id = this.clear;
    let source = this.clearSource;
    let p = null;

    if (this.clearCompiler === null) {
      p = this.compile('clear_compiler')
        .then((repository) => {
          let yaccPromise = this.doSaveYaccLex('yacc', this.yacc, this.yaccSource, repository);
          let lexPromise = this.doSaveYaccLex('lex', this.lex, this.lexSource, repository);

          return Promise.all([yaccPromise, lexPromise]);
        });
    } else {
      p = Promise.resolve();

      if (this.yaccDirty) {
        p = p.then(() => this.doSaveYaccLex('yacc', this.yacc, this.yaccSource, this.clearCompiler));
      }

      if (this.lexDirty) {
        p = p.then(() => this.doSaveYaccLex('lex', this.lex, this.lexSource, this.clearCompiler));
      }
    }

    return p.then(() => this.compile('explainer'))
      .then((explainer) => this.doSaveClear(id, source, explainer, this.clearCompiler, this.yacc, this.lex));
  }

  doSaveClear(id, source, repository, clearCompilerRepository, yacc, lex) {
    this.store.dispatch(startVersionsSave('clear'));

    let out = this.fetchApi('clear/' + id, 'PUT', {
      source: source,
      repository: repository,
      clear_compiler_repository: clearCompilerRepository,
      yacc: yacc,
      lex: lex
    }).then(function (response) {
      return response.json();
    }).then((response) => {
      this.store.dispatch(receiveVersionsSave('clear', response.id, response.version));
      this.getVersions('clear');
    }).then(function () {
      return repository;
    }).catch((e) => {
      this.store.dispatch(failVersionsSave('clear', e.message));
    });

    return out;
  }

  createClear() {
    let source = this.clearSource;
    let p = null;

    if (this.clearCompiler === null) {
      p = this.compile('clear_compiler')
        .then((repository) => {
          let yaccPromise = this.doSaveYaccLex('yacc', this.yacc, this.yaccSource, repository);
          let lexPromise = this.doSaveYaccLex('lex', this.lex, this.lexSource, repository);

          return Promise.all([yaccPromise, lexPromise]);
        });
    } else {
      p = Promise.resolve();

      if (this.yaccDirty) {
        p = p.then(() => this.doSaveYaccLex('yacc', this.yacc, this.yaccSource, this.clearCompiler));
      }

      if (this.lexDirty) {
        p = p.then(() => this.doSaveYaccLex('lex', this.lex, this.lexSource, this.clearCompiler));
      }
    }

    return p.then(() => this.compile('explainer'))
      .then((explainer) => this.doCreateClear(source, explainer, this.clearCompiler, this.yacc, this.lex, this.language));
  }

  doCreateClear(source, repository, clearCompilerRepository, yacc, lex, language) {
    this.store.dispatch(startVersionsCreate('clear'));

    let out = this.fetchApi('clear', 'POST', {
      source: source,
      repository: repository,
      language: language,
      clear_compiler_repository: clearCompilerRepository,
      yacc: yacc,
      lex: lex
    }).then(function (response) {
      return response.json();
    }).then((response) => {
      this.store.dispatch(receiveVersionsCreate('clear', response.id, response.version));
      this.getVersions('clear');
    }).then(function () {
      return repository;
    }).catch((e) => {
      this.store.dispatch(failVersionsCreate('clear', e.message));
    });

    return out;
  }

  compileRegex() {
    let p = Promise.resolve();

    if (this.explainer === null) {
      p = this.saveClear();
    }

    p.then(() => this.store.dispatch(compile(['regex_text', 'tree_text'])));
  }

  saveRegex(name) {
    this.store.dispatch(startVersionsSave('regex'));

    this.fetchApi('regex', 'POST', {
      name: name,
      expression: this.regexSource
    }).then(function (response) {
      return response.json();
    }).then((response) => {
      this.store.dispatch(receiveVersionsSave('regex', response.id, null));
      this.getVersions('regex');
    }).catch((e) => {
      this.store.dispatch(failVersionsCreate('regex', e.message));
    });
  }
}

export default ApiConnection;
