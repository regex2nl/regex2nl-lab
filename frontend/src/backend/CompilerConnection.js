import { compilerConnection } from '../logic/actions';
import {receivedFromCompiler} from "../logic/actions";

class CompilerConnection {

  constructor(store, uri = 'wss://localhost:31410', key = '', reconnect = 10) {
    this.uri = uri;
    this.socket = null;
    this.store = store;
    this.key = key;
    this.sent = false;
    this.previous = { clear: null, explainer: null };
    this.reconnectTime = reconnect * 1000;
    this.firstConnection = true;

    this.store.subscribe(this.onChange.bind(this));
  }

  connect() {
    this.socket = new WebSocket(this.uri);

    this.socket.onmessage = this.onMessage.bind(this);
    this.socket.onclose = this.onClose.bind(this);
    this.socket.onopen = this.onOpen.bind(this);
  }

  onChange() {
    let newState = this.store.getState();

    if (newState.compiler.inProgress != this.sent) {
      this.sent = newState.compiler.inProgress;

      if (this.sent)
        this.send(newState);
    }
  }

  onClose() {
    this.store.dispatch(compilerConnection(false, this.firstConnection));

    this.firstConnection = false;

    setTimeout(() => {
      this.connect();
    }, this.reconnectTime);
  }

  onOpen() {
    this.store.dispatch(compilerConnection(true, this.firstConnection));

    this.firstConnection = false;
  }

  onMessage(event) {
    var msg = JSON.parse(event.data);

    if (msg.type == 'heartbeat'
      || (msg.type == 'compile' && msg.payload.status == 'WORK' && msg.payload.console == ''))
      return;

    this.store.dispatch(receivedFromCompiler(msg.payload, msg.type));
  }

  send(state) {
    if (state.compiler.requestedArtifact != null) {
      this.requestArtifact(state.compiler.requestedArtifact);
      return;
    }

    var input = {
      yacc_source: state.editors.yacc,
      lex_source: state.editors.lex,
      clear_source: state.editors.clear,
      regex_source: state.editors.regex,
    };

    if (state.versions.clearCompiler != null) {
      input.clear_compiler = state.versions.clearCompiler;
    }

    if (state.versions.explainer != null) {
      input.explainer = state.versions.explainer;
    }

    var payload = {
      output: state.compiler.requestedOutput,
      input: input
    };

    this.doSend('compile', payload);
  }

  requestArtifact(artifact) {
    this.doSend('artifact', artifact);
  }

  doSend(type, payload) {
    let msg = {
      type: type,
      auth: this.key,
      payload: payload
    };

    this.socket.send(JSON.stringify(msg));
  }
}

export default CompilerConnection;
