/**
 * Created by eplightning on 21.11.2016.
 */

import React, { PropTypes } from 'react';

import './console.css';

class Console extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <textarea value={this.props.value} className="output-console">

      </textarea>
    );
  }

}

Console.defaultProps = {
  value: ''
};

export default Console;
