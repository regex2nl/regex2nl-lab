/**
 * Created by Bartosz Adamski on 29.11.16.
 */

import GridBlock from '../GridBlock';
import React, { PropTypes } from 'react';

import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import YaccLexDialog from '../YaccLexDialog';
import LoadFileDialog from '../LoadFileDialog';

class YaccGridBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogOpened: false,
      fileLoadDialogOpened: false,
      selected: 0
    };

    this.onRequestOpenDialog = this.onRequestOpenDialog.bind(this);
    this.onRequestCloseDialog = this.onRequestCloseDialog.bind(this);
    this.onChangeVersion = this.onChangeVersion.bind(this);
    this.onRequestCloseFileLoadDialog = this.onRequestCloseFileLoadDialog.bind(this);
    this.onRequestOpenFileLoadDialog = this.onRequestOpenFileLoadDialog.bind(this);
  }

  onRequestOpenDialog(version) {
    this.setState({dialogOpened: true, selected: version});
  }

  onRequestCloseDialog() {
    this.setState({dialogOpened: false, selected: 0});
  }
    
  onRequestOpenFileLoadDialog() {
    this.setState({fileLoadDialogOpened: true});
  }

  onRequestCloseFileLoadDialog() {
    this.setState({fileLoadDialogOpened: false});
  }

  onChangeVersion(event, index, val) {
    this.setState({selected: val});
  }

  render() {
    let childHeight = parseInt(this.props.style.height) - 65;

    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        height: childHeight
      }));

    let {onCompile, availableVersions, ...otherProps} = this.props;

    return (
      <GridBlock {...otherProps} onRequestOpenDialog={() => this.onRequestOpenDialog(this.props.version)}
                                 title={this.props.title + " " + this.props.versionLabel} onCompile={onCompile}
                                 onRequestOpenFileLoadDialog={this.onRequestOpenFileLoadDialog}>
        <div>
          <LoadFileDialog
            open={this.state.fileLoadDialogOpened}
            title={"Select a file to load"}
            onDialogSubmit={this.props.onFileLoad}
            onRequestClose={this.onRequestCloseFileLoadDialog} />
            
          <YaccLexDialog
            open={this.state.dialogOpened}
            version = {this.state.selected}
            title = {"Load " + this.props.title}
            availableVersions={this.props.availableVersions}
            onChange={this.onChangeVersion}
            onDialogSubmit={this.props.onChange}
            onRequestClose={this.onRequestCloseDialog} />
        </div>
        {children}
      </GridBlock>
    );
  }
}

export default YaccGridBlock;
