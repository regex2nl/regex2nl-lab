/**
 * Created by Bartosz Adamski on 06.12.16.
 */
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';

export default class NewRegexDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      regexName: '',
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={this.props.onRequestClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => {this.props.onRequestClose(); this.props.onCreateRegex(this.state.regexName);}} // API wywoalnie this.context?
      />,
    ];

    const newProps = { ...this.props,
      actions: actions,
      modal: false
    };

    return (
      <Dialog {...newProps}>
        <TextField hintText="Enter regex name" value={this.state.regexName}
                    onChange={(event) => {this.setState({regexName: event.target.value})}}>
        </TextField>
      </Dialog>
    );
  }
}
