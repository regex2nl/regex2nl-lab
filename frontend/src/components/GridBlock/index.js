/**
 * Created by eplightning on 21.11.2016.
 */

import React, { PropTypes } from 'react';

import FontIcon from 'material-ui/FontIcon';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import YaccLexDialog from '../YaccLexDialog';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';

class GridBlock extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let childHeight = parseInt(this.props.style.height) - 65;

    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        height: childHeight
      }));

    let {onCompile, onFullscreen, ...otherProps} = this.props;

    return (
      <div {...otherProps}>

        <Paper transitionEnabled={false} zDepth={1} style={{height: this.props.style.height, width: this.props.style.width}}>
          <Toolbar className="gridHandle">
            <ToolbarGroup>
              <ToolbarTitle text={this.props.title} />
            </ToolbarGroup>
            <ToolbarGroup lastChild={false}>
              <IconMenu
                iconButtonElement={
              <IconButton touch={true}>
                <FontIcon className="material-icons">
                  more_vert
                </FontIcon>
              </IconButton>
            }
              >
                <MenuItem primaryText="New version" onTouchTap={this.props.onCreateVersion}/>
                <MenuItem primaryText="Load version" onTouchTap={this.props.onRequestOpenDialog}/>
                <MenuItem primaryText="Upload file" onTouchTap={this.props.onRequestOpenFileLoadDialog}/>
              </IconMenu>
              <IconButton onTouchTap={onFullscreen}>
                <FontIcon className="material-icons">
                  fullscreen
                </FontIcon>
              </IconButton>
              <IconButton onTouchTap={onCompile}>
                <FontIcon className="material-icons">
                  send
                </FontIcon>
              </IconButton>
            </ToolbarGroup>
          </Toolbar>
          {children}
        </Paper>
      </div>
    );
  }

}

GridBlock.defaultProps = {
  onCompile: function () {},
  onFullscreen: function () {},
  onChange: function (val) {},
  title: '',
};

export default GridBlock;
