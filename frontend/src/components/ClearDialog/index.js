/**
 * Created by Bartosz Adamski on 06.12.16.
 */
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Checkbox from 'material-ui/Checkbox';

export default class ClearDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadLexYacc: true,
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={this.props.onRequestClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => {this.props.onRequestClose(); this.props.onDialogSubmit(this.props.versionSelected, this.state.loadLexYacc);}}
      />,
    ];

    let languages = new Map();

    for (let key of this.props.availableVersions)
      languages.set(key.language.alpha2, key.language.name);

    let items = [];
    languages.forEach(function (label, key) {
      items.push(<MenuItem key={key} value={key} primaryText={label} />);
    });

    let items2 = [];
    for (let version of this.props.availableVersions) {
      if (version.language.alpha2 == this.props.language){
        let id = version.id;
        items2.push(<MenuItem key={id} value={id} primaryText={version.version} />);
      }
    }

    const newProps = { ...this.props,
      title: "CLEAR",
      actions: actions,
      modal: false
    };

    return (
      <Dialog {...newProps}>
        <h4>Choose language</h4>
        <SelectField
          value = {this.props.language}
          onChange={this.props.onChangeLanguage}
          maxHeight={200}
        >
          {items}
        </SelectField>
        <br/>
        <h4>Choose version</h4>
        <SelectField
          value = {this.props.versionSelected}
          onChange={this.props.onChangeVersion}
          maxHeight={200}
        >
          {items2}
        </SelectField>
        <Checkbox
          label="Load LEX and YACC for this CLEAR version"
          checked={this.state.loadLexYacc}
          onCheck = {(event, isInputChecked) =>{
            this.setState({loadLexYacc: isInputChecked})
          }}
        />
      </Dialog>
    );
  }
}
