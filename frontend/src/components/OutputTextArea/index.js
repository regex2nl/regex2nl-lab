/**
 * Created by Adamski Bartosz on 20.12.16.
 */

import React, { PropTypes } from 'react';

class OutputTextArea extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let childHeight = parseInt(this.props.height) - 170;

    return (
      <textarea className={this.props.className} style={{height: childHeight}} placeholder = "Tree" disabled value={this.props.regexOutput}/>
    );
  }
}

export default OutputTextArea;
