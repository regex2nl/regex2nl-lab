/**
 * Created by Bartosz Adamski on 06.12.16.
 */
import GridBlock from '../GridBlock';
import React, { PropTypes } from 'react';

import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import YaccLexDialog from '../YaccLexDialog';
import NewRegexDialog from '../NewRegexDialog';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import LoadRegexDialog from '../LoadRegexDialog';

class RegexGridBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newRegexDialogOpened: false,
      loadRegexDialogOpened: false,
      selected: 0,
    };

    this.onRequestOpenLoadRegexDialog = this.onRequestOpenLoadRegexDialog.bind(this);
    this.onRequestOpenNewRegexDialog = this.onRequestOpenNewRegexDialog.bind(this);
    this.onRequestCloseLoadRegexDialog = this.onRequestCloseLoadRegexDialog.bind(this);
    this.onRequestCloseNewRegexDialog = this.onRequestCloseNewRegexDialog.bind(this);
    this.onChangeVersion = this.onChangeVersion.bind(this);
  }

  onRequestOpenLoadRegexDialog(version) {
    this.setState({loadRegexDialogOpened: true, selected: version});
  }

  onRequestOpenNewRegexDialog(version) {
    this.setState({newRegexDialogOpened: true, selected: version});
  }

  onRequestCloseLoadRegexDialog() {
    this.setState({loadRegexDialogOpened: false, selected: 0});
  }

  onRequestCloseNewRegexDialog() {
    this.setState({newRegexDialogOpened: false, selected: 0});
  }

  onChangeVersion(event, index, val) {
    this.setState({selected: val});
  }

  render() {
    let childHeight = parseInt(this.props.style.height) - 65;

    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        height: childHeight
      }));

    let {onCompile, onFullscreen, expressions, ...otherProps} = this.props;

    return (
      <div {...otherProps}>
        <Paper transitionEnabled={false} zDepth={1} style={{height: this.props.style.height, width: this.props.style.width}}>
          <Toolbar className="gridHandle">
            <ToolbarGroup>
              <ToolbarTitle text={this.props.title} />
            </ToolbarGroup>
            <ToolbarGroup lastChild={false}>
              <IconMenu
                iconButtonElement={
                  <IconButton touch={true}>
                    <FontIcon className="material-icons">
                      more_vert
                    </FontIcon>
                  </IconButton>
                }
              >
                <MenuItem primaryText="New regex" onTouchTap={this.onRequestOpenNewRegexDialog}/>
                <MenuItem primaryText="Load regex" onTouchTap={this.onRequestOpenLoadRegexDialog}/>
              </IconMenu>

              <IconButton onTouchTap={onFullscreen}>
                <FontIcon className="material-icons">
                  fullscreen
                </FontIcon>
              </IconButton>

              <IconButton onTouchTap={onCompile}>
                <FontIcon className="material-icons">
                  send
                </FontIcon>
              </IconButton>
            </ToolbarGroup>
          </Toolbar>

          <NewRegexDialog title={'New ' + this.props.title}
                          open = {this.state.newRegexDialogOpened}
                          modal={false}
                          onRequestClose={this.onRequestCloseNewRegexDialog}
                          onCreateRegex = {this.props.onCreateRegex}/>

          <LoadRegexDialog
            open={this.state.loadRegexDialogOpened}
            version = {this.state.selected}
            title = {'Load ' + this.props.title}
            onChange={this.onChangeVersion}
            onChooseRegex={this.props.onChooseRegex}
            onRequestClose={this.onRequestCloseLoadRegexDialog}
            availableVersions={this.props.availableVersions}
          />

          {children}
        </Paper>
      </div>
    );
  }
}

export default RegexGridBlock;
