/**
 * Created by Bartosz Adamski on 06.12.16.
 */

import GridBlock from '../GridBlock';
import React, { PropTypes } from 'react';

import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import ClearDialog from '../ClearDialog';
import LoadFileDialog from '../LoadFileDialog';

class ClearGridBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogOpened: false,
      versionSelected: 0,
      languageSelected: '',
      fileLoadDialogOpened: false,
    };

    this.onRequestOpenDialog = this.onRequestOpenDialog.bind(this);
    this.onRequestCloseDialog = this.onRequestCloseDialog.bind(this);
    this.onChangeVersion = this.onChangeVersion.bind(this);
    this.onChangeLanguage = this.onChangeLanguage.bind(this);    
    this.onRequestCloseFileLoadDialog = this.onRequestCloseFileLoadDialog.bind(this);
    this.onRequestOpenFileLoadDialog = this.onRequestOpenFileLoadDialog.bind(this);
  }

  onRequestOpenDialog(version) {
    let language = 'pl';

    for (let v of this.props.availableVersions) {
      if (v.id === version)
        language = v.language.alpha2;
    }

    this.setState({dialogOpened: true, versionSelected: version, languageSelected: language});
  }

  onRequestCloseDialog() {
    this.setState({dialogOpened: false, versionSelected: 0, languageSelected: ''});
  }

  onChangeVersion(event, index, val) {
    this.setState({versionSelected: val});
  }

  onRequestOpenFileLoadDialog() {
    this.setState({fileLoadDialogOpened: true});
  }

  onRequestCloseFileLoadDialog() {
    this.setState({fileLoadDialogOpened: false});
  }
  
  onChangeLanguage(event, index, val) {
    const version = this.props.availableVersions.filter((element) => {
      return element.language.alpha2 === val;
    }).reduce((prev, current) => {
      return (prev.version > current.version) ? prev : current;
    });

    this.setState({languageSelected: val, versionSelected: version.id});
  }

  render() {
    let childHeight = parseInt(this.props.style.height) - 65;

    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        height: childHeight
      }));

    let {onCompile, availableVersions, ...otherProps} = this.props;

    return (
      <GridBlock {...otherProps} onRequestOpenDialog={() => this.onRequestOpenDialog(this.props.version)}
                                 title={this.props.title + " " + this.props.versionLabel} onCompile={onCompile}
                                 onRequestOpenFileLoadDialog={this.onRequestOpenFileLoadDialog}>
        <div>
          <LoadFileDialog
            open={this.state.fileLoadDialogOpened}
            title={"Select a file to load"}
            onDialogSubmit={this.props.onFileLoad}
            onRequestClose={this.onRequestCloseFileLoadDialog} />
            
          <ClearDialog
            open={this.state.dialogOpened}
            versionSelected = {this.state.versionSelected}
            language = {this.state.languageSelected}
            availableVersions={this.props.availableVersions}
            onChangeVersion={this.onChangeVersion}
            onChangeLanguage={this.onChangeLanguage}
            onDialogSubmit={this.props.onChange}
            onRequestClose={this.onRequestCloseDialog} />
        </div>
        {children}
      </GridBlock>
    );
  }
}

export default ClearGridBlock;
