/**
 * Created by Bartosz Adamski on 13.12.16.
 */

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

export default class LoadRegexDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRegex: '',
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={this.props.onRequestClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => {this.props.onChooseRegex(this.state.currentRegex); this.props.onRequestClose();
        }}
      />,
    ];

    let items = [];

    for (let version of this.props.availableVersions) {
      items.push(<MenuItem key={version.expression} value={version.expression} primaryText={version.name} />);
    }

    const newProps = { ...this.props,
      actions: actions,
      modal: false
    };

    return (
      <Dialog {...newProps}>
        <SelectField
          value = {this.props.version}
          onChange={(event, key, payload) => {this.props.onChange(event, key, payload); this.setState({currentRegex:payload});}}
          maxHeight={200}
        >
          {items}
        </SelectField>

      </Dialog>
    );
  }
}
