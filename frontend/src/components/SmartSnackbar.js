import React, { Component } from 'react';

import Snackbar from 'material-ui/Snackbar';

export default class SmartSnackbar extends Component {

  constructor(props) {
    super(props);

    this.state = {};
    this.isOpen = false;
    this.previousSnack = '';
    this.onSnackbarRequestClose = this.onSnackbarRequestClose.bind(this);
  }

  onSnackbarRequestClose() {
    this.isOpen = false;
    this.setState({});
  }

  render() {
    this.isOpen = (this.isOpen || this.props.message != this.previousSnack) && (this.props.message != '');

    this.previousSnack = this.props.message;

    return (
      <Snackbar message={this.props.message} open={this.isOpen}
                action="Zamknij" onActionTouchTap={this.onSnackbarRequestClose}
                onRequestClose={this.onSnackbarRequestClose} autoHideDuration={5000} />
    );
  }
}
