/**
 * Created by eplightning on 21.11.2016.
 */

import React, { PropTypes } from 'react';

import AceEditor from 'react-ace';

class WrappedAceEditor extends React.Component {

  constructor(props) {
    super(props);

    this.aceEditor = null;
    this.onAceLoad = this.onAceLoad.bind(this);
  }

  componentDidUpdate() {
    if (this.aceEditor != null)
      this.aceEditor.resize();
  }

  onAceLoad(editor) {
    this.aceEditor = editor;
  }

  render() {
    return (
      <AceEditor name={this.props.name}
                 mode={this.props.mode} theme={this.props.theme} height={this.props.height + 'px'}
                 width="100%"
                 editorProps={{$blockScrolling: false}}
                 onChange={this.props.onChange}
                 onLoad={this.onAceLoad}
                 value={this.props.value} />
    );
  }
}

WrappedAceEditor.propTypes = {
  name: PropTypes.string.isRequired,
  height: PropTypes.any,
  onChange: PropTypes.func,
  value: PropTypes.string,
  theme: PropTypes.string,
  mode: PropTypes.string
};

WrappedAceEditor.defaultProps = {
  onChange: function (value) {},
  value: '',
  theme: 'chrome',
  mode: 'yacc',
  height: 100
};

export default WrappedAceEditor;
