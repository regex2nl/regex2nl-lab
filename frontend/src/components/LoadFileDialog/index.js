/**
 * Created by Bartosz Adamski on 15.01.17.
 */
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';

export default class LoadFileDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fileContent: '',
    };
    
    this.loadFileAsText = this.loadFileAsText.bind(this);
  }  
  
 loadFileAsText(){
   if(this.fileInput.files.length == 1){
      var fileToLoad = this.fileInput.files[0];
      var fileReader = new FileReader();
      var fileDialog = this;
      fileReader.onload = function(fileLoadedEvent){
          fileDialog.setState({fileContent: fileLoadedEvent.target.result});
          fileDialog.props.onDialogSubmit(fileDialog.state.fileContent);
      };   
      fileReader.readAsText(fileToLoad, "UTF-8");
    }
 }
    
  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={() => {this.props.onRequestClose(); this.setState({fileContent: ''}); }}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => {this.loadFileAsText(); this.props.onRequestClose(); this.setState({fileContent: ''}); }}
      />,
    ];

    const newProps = { ...this.props,
      actions: actions,
      modal: false
    };

    return (
      <Dialog {...newProps}>        
        <input type="file" ref={(input) => {this.fileInput = input;}} />
      </Dialog>
    );
  }
}
