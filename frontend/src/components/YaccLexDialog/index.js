/**
 * Created by Bartosz Adamski on 29.11.16.
 */
import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

export default class YaccLexDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={this.props.onRequestClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => {this.props.onRequestClose(); this.props.onDialogSubmit(this.props.version);}}
      />,
    ];

    let items = [];

    for (let version of this.props.availableVersions) {
      items.push(<MenuItem key={version.id} value={version.id} primaryText={version.version} />);
    }

    const newProps = { ...this.props,
      actions: actions,
      modal: false
    };

    return (
      <Dialog {...newProps}>
        <SelectField
          value = {this.props.version}
          onChange={this.props.onChange}
          maxHeight={200}
        >
          {items}
        </SelectField>

      </Dialog>
    );
  }
}
