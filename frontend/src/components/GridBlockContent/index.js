/**
 * Created by eplightning on 21.11.2016.
 */

import React, { PropTypes } from 'react';

import './block-content.css';

class GridBlockContent extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        height: this.props.height - this.props.paddingV * 2
      }));

    return (
      <div className="grid-block-content" style={{ height: this.props.height+'px', padding: this.props.paddingV+'px '+this.props.paddingH+'px' }}>
        {children}
      </div>
    );
  }

}

GridBlockContent.defaultProps = {
  height: 100,
  paddingV: 0,
  paddingH: 0,
  style: {}
};

export default GridBlockContent;
