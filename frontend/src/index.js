import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

import CompilerConnection from './backend/CompilerConnection';

import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './logic/reducers';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {receiveVersions} from "./logic/actions";
import ApiConnection from "./backend/ApiConnection";
import AutosaveManager from "./logic/AutosaveManager";

injectTapEventPlugin();

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

let compilerSocket = new CompilerConnection(store, 'wss://sds3.cs.put.poznan.pl:443/compiler', window.COMPILE_SERVER_AUTH_KEY);
//let compilerSocket = new CompilerConnection(store, 'ws://localhost:31410', window.COMPILE_SERVER_AUTH_KEY);

compilerSocket.connect();

let api = new ApiConnection(store, 'https://sds3.cs.put.poznan.pl/api/');
//let api = new ApiConnection(store, 'http://localhost:8000/api/');

let autosaveManager = new AutosaveManager(store, api);

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider>
      <App api={api} autosaveManager={autosaveManager} />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);

api.getVersions('yacc');
api.getVersions('lex');
api.getVersions('clear', true);
api.getVersions('regex');
