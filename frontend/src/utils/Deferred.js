// https://developer.mozilla.org/en-US/docs/Mozilla/JavaScript_code_modules/Promise.jsm/Deferred

export default function Deferred() {
  this.resolve = null;
  this.reject = null;

  this.promise = new Promise(function (resolve, reject) {
    this.resolve = resolve;
    this.reject = reject;
  }.bind(this));

  Object.freeze(this);
}
