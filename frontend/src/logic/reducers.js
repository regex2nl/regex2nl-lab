import { UPDATE_EDITOR_VALUE, COMPILE, VERSIONS_CREATE_START, VERSIONS_CREATE_RECEIVE, FETCH_ARTIFACT} from './actions';
import { combineReducers } from 'redux';
import {COMPILE_RESPONSE_WORK} from "./actions";
import {COMPILE_RESPONSE_FAILURE} from "./actions";
import {COMPILE_RESPONSE_SUCCESS} from "./actions";
import {COMPILE_RESPONSE_ARTIFACT} from "./actions";
import {COMPILE_CONNECTION_STATE} from "./actions";
import {VERSIONS_GET_RECEIVE, VERSIONS_GET_START} from "./actions";
import {VERSIONS_LOAD_RECEIVE, VERSIONS_LOAD_START} from "./actions";
import {VERSIONS_SAVE_START} from "./actions";
import {VERSIONS_SAVE_RECEIVE, AUTOSAVE_DATA_UPDATED} from "./actions";

const initialState = {
  editors: {
    yacc: '',
    lex: '',
    clear: '',
    regex: ''
  },
  compiler: {
    inProgress: false,
    connected: false,
    requestedOutput: null,
    requestedArtifact: null,
    output: null,
    artifact: '',
    artifacts: [ ],
    console: '',
    notice: ''
  },
  regex: {
    explanation: '',
    tree: ''
  },
  autosave: {
    versions: [],
  },
  versions: {
    // lista wersji
    yacc: [],
    lex: [],
    clear: [],
    regex: [],

    // wybory
    yaccSelected: 0,
    lexSelected: 0,
    clearSelected: 0,

    // obecnie wybrana wersja (do wyświetlenia)
    yaccLabel: '0',
    lexLabel: '0',
    clearLabel: '0',

    // obecny język
    clearLanguage: 'pl',

    // czy były zmiany
    yaccDirty: true,
    lexDirty: true,

    // identyfikatory kompilacji
    clearCompiler: null,
    explainer: null,

    // zależności wczytanego Cleara
    yaccDependency: null,
    lexDependency: null,

    // czy zapytanie API in progress
    getInProgress: 0,
    loadInProgress: 0,
    saveInProgress: 0,
    createInProgress: 0
  }
};


function versionReducer(state = initialState.versions, action) {
  switch (action.type) {
    case VERSIONS_GET_RECEIVE:
      let inProgress = state.getInProgress > 0 ? state.getInProgress - 1 : 0;

      return { ...state, [action.payload.type]: action.payload.versions, getInProgress: inProgress };

    case VERSIONS_LOAD_RECEIVE:
      let inProgressLoad = state.loadInProgress > 0 ? state.loadInProgress - 1 : 0;

      let newState = { ...state, loadInProgress: inProgressLoad };

      if (action.payload.type == 'clear') {
        newState.clearSelected = action.payload.id;
        newState.clearLabel = action.payload.version;
        newState.clearLanguage = action.payload.language;

        if (action.payload.full) {
          newState.explainer = action.payload.repository;
          newState.clearCompiler = action.payload.clearCompilerRepository;
        }

        newState.yaccDependency = action.payload.dependencies.yacc;
        newState.lexDependency = action.payload.dependencies.lex;
      } else if (action.payload.type == 'lex' || action.payload.type == 'yacc') {
        newState[action.payload.type + 'Selected'] = action.payload.id;
        newState[action.payload.type + 'Label'] = action.payload.version;
        newState[action.payload.type + 'Dirty'] = false;

        if (state[action.payload.type + 'Dependency'] != action.payload.id) {
          newState.explainer = null;
          newState.clearCompiler = null;
        }
      }

      return newState;

    case VERSIONS_GET_START:
      return { ...state, getInProgress: state.getInProgress + 1 };

    case VERSIONS_LOAD_START:
      return { ...state, loadInProgress: state.loadInProgress + 1 };

    case VERSIONS_SAVE_START:
      return { ...state, saveInProgress: state.saveInProgress + 1 };

    case VERSIONS_CREATE_START:
      return { ...state, createInProgress: state.createInProgress + 1 };

    case VERSIONS_CREATE_RECEIVE:
    case VERSIONS_SAVE_RECEIVE:
      let newState2 = { ...state };

      if (action.type == VERSIONS_CREATE_RECEIVE) {
        newState2.createInProgress = state.createInProgress > 0 ? state.createInProgress - 1 : 0;
      } else {
        newState2.saveInProgress = state.saveInProgress > 0 ? state.saveInProgress - 1 : 0;
      }

      if (action.error)
        return newState2;

      if (action.payload.type != 'regex') {
        newState2[action.payload.type + 'Selected'] = action.payload.id;
        newState2[action.payload.type + 'Label'] = action.payload.version;
      }

      if (action.payload.type == 'lex' || action.payload.type == 'yacc') {
        newState2[action.payload.type + 'Dirty'] = false;
      }

      if (action.payload.type == 'clear') {
        newState2.yaccDependency = newState2.yaccSelected;
        newState2.lexDependency = newState2.lexSelected;
      }

      return newState2;

    case UPDATE_EDITOR_VALUE:
      if (action.payload.editor == 'clear') {
        return { ...state, explainer: null };
      } else if (action.payload.editor == 'yacc' || action.payload.editor == 'lex') {
        return { ...state, clearCompiler: null, explainer: null, [action.payload.editor + 'Dirty']: true };
      } else {
        return state;
      }

    case COMPILE_RESPONSE_SUCCESS:
      if ('explainer' in action.payload.output)
        return { ...state, explainer: action.payload.output.explainer };
      else if ('clear_compiler' in action.payload.output)
        return { ...state, clearCompiler: action.payload.output.clear_compiler };
      else
        return state;

    default:
      return state;
  }
}

function editorReducer(state = initialState.editors, action) {
  switch (action.type) {
    case UPDATE_EDITOR_VALUE:
      return { ...state, [action.payload.editor]: action.payload.value };

    case VERSIONS_LOAD_RECEIVE:
      return { ...state, [action.payload.type]: action.payload.source };

    default:
      return state;
  }
}

function regexReducer(state = initialState.regex, action) {
  if (action.type == COMPILE_RESPONSE_SUCCESS) {
    if ('regex_text' in action.payload.output && 'tree_text' in action.payload.output)
      return { explanation: action.payload.output.regex_text, tree: action.payload.output.tree_text };
  }

  return state;
}

function compilerReducer(state = initialState.compiler, action) {
  switch (action.type) {
    case COMPILE:
      if (state.inProgress) {
        return { ...state, notice: 'Proces kompilacji jest w trakcie' };
      } else if (!state.connected) {
        return { ...state, notice: 'Brak połączenia z serwerem' };
      } else {
        return { ...state, notice: '', inProgress: true, requestedOutput: action.payload.requestedOutput, requestedArtifact: null };
      }

    case FETCH_ARTIFACT:
      if (state.inProgress) {
        return { ...state, notice: 'Proces kompilacji jest w trakcie' };
      } else if (!state.connected) {
        return { ...state, notice: 'Brak połączenia z serwerem' };
      } else {
        return { ...state, notice: '', inProgress: true, requestedOutput: null, requestedArtifact: action.payload.artifact };
      }

    case COMPILE_RESPONSE_WORK:
      return { ...state, console: state.console + action.payload.console };

    case COMPILE_RESPONSE_FAILURE:
      return {
        ...state,
        console: state.console + action.payload.console,
        notice: 'Proces kompilacji zakończony niepowodzeniem',
        inProgress: false,
        output: null
      };

    case COMPILE_RESPONSE_SUCCESS:
      return {
        ...state,
        console: state.console + action.payload.console,
        notice: 'Proces kompilacji zakończony pomyślnie',
        inProgress: false,
        output: action.payload.output,
        artifacts: action.payload.artifacts,
        artifact: ''
      };

    case COMPILE_RESPONSE_ARTIFACT:
      return {
        ...state,
        inProgress: false,
        artifact: action.payload.artifact
      };

    case COMPILE_CONNECTION_STATE:
      if (action.payload.first)
        return {
          ...state,
          notice: action.payload.state ? 'Połączono z serwerem kompilacji' : 'Nie udało połączyć się z serwerem kompilacji',
          connected: action.payload.state
        };

      if (action.payload.state == state.connected)
        return state;

      return {
        ...state,
        notice: action.payload.state ? 'Połączono z serwerem kompilacji' : 'Utracono połączenie z serwerem kompilacji',
        connected: action.payload.state,
        inProgress: state.inProgress && action.payload.state,
        output: action.payload.state ? state.output : null
      };

    default:
      return state;
  }
}

function autosaveReducer(state = initialState.autosave, action) {
 if (action.type == AUTOSAVE_DATA_UPDATED) {
   return { versions: action.payload.versions };
 }

 return state;
}

let rootReducer = combineReducers({
  editors: editorReducer,
  compiler: compilerReducer,
  versions: versionReducer,
  regex: regexReducer,
  autosave: autosaveReducer
});

export default rootReducer;
