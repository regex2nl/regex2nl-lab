export const UPDATE_EDITOR_VALUE = 'UPDATE_EDITOR_VALUE';

export const COMPILE = 'COMPILE';
export const FETCH_ARTIFACT = 'FETCH_ARTIFACT';
export const COMPILE_RESPONSE_WORK = 'COMPILE_RESPONSE_WORK';
export const COMPILE_RESPONSE_ARTIFACT = 'COMPILE_RESPONSE_ARTIFACT';
export const COMPILE_RESPONSE_FAILURE = 'COMPILE_RESPONSE_FAILURE';
export const COMPILE_RESPONSE_SUCCESS = 'COMPILE_RESPONSE_SUCCESS';
export const COMPILE_CONNECTION_STATE = 'COMPILE_CONNECTION_STATE';

export const VERSIONS_GET_RECEIVE = 'VERSIONS_GET_RECEIVE';
export const VERSIONS_LOAD_RECEIVE = 'VERSIONS_LOAD_RECEIVE';
export const VERSIONS_GET_START = 'VERSIONS_GET_START';
export const VERSIONS_LOAD_START = 'VERSIONS_LOAD_START';
export const VERSIONS_SAVE_START = 'VERSIONS_SAVE_START';
export const VERSIONS_SAVE_RECEIVE = 'VERSIONS_SAVE_RECEIVE';
export const VERSIONS_CREATE_START = 'VERSIONS_CREATE_START';
export const VERSIONS_CREATE_RECEIVE = 'VERSIONS_CREATE_RECEIVE';

export const AUTOSAVE_DATA_UPDATED = 'AUTOSAVE_DATA_UPDATED';

export function autosaveUpdate(versions, firstUpdate = false) {
  return {
    type: AUTOSAVE_DATA_UPDATED,
    payload: {
      versions: versions,
      firstUpdate: firstUpdate
    }
  }
}

export function updateEditor(editor, value) {
  return {
    type: UPDATE_EDITOR_VALUE,
    payload: {
      editor: editor,
      value: value
    }
  };
}

export function receiveVersionsCreate(type, id, version) {
  return {
    type: VERSIONS_CREATE_RECEIVE,
    payload: {
      type: type,
      id: id,
      version: version
    }
  };
}

export function failVersionsCreate(type, message) {
  return {
    type: VERSIONS_CREATE_RECEIVE,
    error: true,
    payload: {
      type: type,
      message: message
    }
  };
}

export function startVersionsCreate(type) {
  return {
    type: VERSIONS_CREATE_START,
    payload: {
      type: type
    }
  }
}

export function receiveVersionsSave(type, id, version) {
  return {
    type: VERSIONS_SAVE_RECEIVE,
    payload: {
      type: type,
      id: id,
      version: version
    }
  };
}

export function failVersionsSave(type, message) {
  return {
    type: VERSIONS_SAVE_RECEIVE,
    error: true,
    payload: {
      type: type,
      message: message
    }
  };
}

export function receiveVersionsGet(type, versions) {
  return {
    type: VERSIONS_GET_RECEIVE,
    payload: {
      type: type,
      versions: versions
    }
  };
}

export function failVersionsGet(type, message) {
  return {
    type: VERSIONS_GET_RECEIVE,
    error: true,
    payload: {
      type: type,
      message: message
    }
  };
}

export function receiveVersionsLoad(type, id, version, language, source, repository, dependencies, clearCompilerRepository, full) {
  return {
    type: VERSIONS_LOAD_RECEIVE,
    payload: {
      type: type,
      id: id,
      version: version,
      language: language,
      source: source,
      repository: repository,
      clearCompilerRepository: clearCompilerRepository,
      dependencies: dependencies,
      full: full
    }
  };
}

export function failVersionsLoad(type, message) {
  return {
    type: VERSIONS_LOAD_RECEIVE,
    error: true,
    payload: {
      type: type,
      message: message
    }
  };
}

export function startVersionsGet(type) {
  return {
    type: VERSIONS_GET_START,
    payload: {
      type: type
    }
  }
}

export function startVersionsLoad(type) {
  return {
    type: VERSIONS_LOAD_START,
    payload: {
      type: type
    }
  }
}

export function startVersionsSave(type) {
  return {
    type: VERSIONS_SAVE_START,
    payload: {
      type: type
    }
  }
}

export function compile(requestedOutput, deferred = null) {
  return {
    type: COMPILE,
    payload: {
      requestedOutput: requestedOutput,
      deferred: deferred
    }
  };
}

export function fetchArtifact(artifact) {
  return {
    type: FETCH_ARTIFACT,
    payload: {
      artifact: artifact
    }
  };
}

export function receivedFromCompiler(msg, type) {
  if (type == 'artifact') {
    return {
      type: COMPILE_RESPONSE_ARTIFACT,
      payload: {
        artifact: msg.data
      }
    };
  }

  switch (msg.status) {
    case 'WORK':
      return {
        type: COMPILE_RESPONSE_WORK,
        payload: {
          console: msg.console
        }
      };

    case 'FAILURE':
      return {
        type: COMPILE_RESPONSE_FAILURE,
        payload: {
          console: msg.console
        }
      };

    case 'SUCCESS':
      return {
        type: COMPILE_RESPONSE_SUCCESS,
        payload: {
          console: msg.console,
          output: msg.output,
          artifacts: msg.artifacts
        }
      };

    default:
      // ??
  }
}

export function compilerConnection(state, first) {
  return {
    type: COMPILE_CONNECTION_STATE,
    payload: {
      state: state,
      first: first
    }
  };
}
