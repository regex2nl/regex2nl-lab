import {autosaveUpdate, updateEditor} from "./actions";
let localStorage = window.localStorage;

class AutosaveManager {

  constructor(store, api, everySeconds = 10) {
    this.store = store;
    this.api = api;

    if (localStorage.getItem('autosave-data')) {
      this.loadData(JSON.parse(localStorage.getItem('autosave-data')));
    } else {
      this.data = [];
      this.index = 0;
    }

    this.yaccSource = '';
    this.lexSource = '';
    this.clearSource = '';
    this.regexSource = '';
    this.yacc = null;
    this.lex = null;
    this.clear = null;

    this.store.subscribe(this.onChange.bind(this));
    setInterval(() => this.onTimer(), everySeconds * 1000);
  }

  loadData(data) {
    this.data = data;
    this.index = Math.max(...Object.keys(data)) + 1;

    if (this.index > 5) {
      this.data.shift();
      this.index -= 1;
    }

    this.store.dispatch(autosaveUpdate(this.prepareData(this.data), true));
  }

  prepareData(data) {
    let map = {};

    for (let k in this.data) {
      map[k] = (new Date(this.data[k].date)).toLocaleString();
    }

    return map;
  }

  onTimer() {
    if (!this.yacc || !this.lex || !this.clear)
      return;

    this.data[this.index] = {
      source: {
        yacc: this.yaccSource,
        lex: this.lexSource,
        clear: this.clearSource,
        regex: this.regexSource
      },
      versions: {
        yacc: this.yacc,
        lex: this.lex,
        clear: this.clear
      },
      date: Date.now()
    };

    localStorage.setItem('autosave-data', JSON.stringify(this.data));

    this.store.dispatch(autosaveUpdate(this.prepareData(this.data)));
  }

  load(version) {
    let data = this.data[version];

    if (!data)
      return;

    let loads = Promise.all([
      this.api.loadVersion('yacc', data.versions.yacc),
      this.api.loadVersion('lex', data.versions.lex),
      this.api.loadVersion('clear', data.versions.clear)
    ]);

    loads.then(() => {
      this.store.dispatch(updateEditor('yacc', data.source.yacc));
      this.store.dispatch(updateEditor('lex', data.source.lex));
      this.store.dispatch(updateEditor('clear', data.source.clear));
      this.store.dispatch(updateEditor('regex', data.source.regex));
    });
  }

  onChange() {
    let newState = this.store.getState();

    this.yaccSource = newState.editors.yacc;
    this.lexSource = newState.editors.lex;
    this.clearSource = newState.editors.clear;
    this.regexSource = newState.editors.regex;
    this.yacc = newState.versions.yaccSelected;
    this.lex = newState.versions.lexSelected;
    this.clear = newState.versions.clearSelected;
  }
}

export default AutosaveManager;
