'use strict';

const header = `var acequire = require('brace').acequire;`;

module.exports = function (src) {
  var fixed = src;
  fixed = fixed
    .replace(/require/g, 'acequire')
    .replace(/define\(/g, 'ace.define(')
    .replace(/['"]acequire['"]/g, '"require"')
    .replace(/emmet\.acequire/g, 'emmet.require');

  return header + "\n" + fixed;
};
