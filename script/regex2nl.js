var RegexEmbed = {};

RegexEmbed.guessLanguage = function() {
    // browser language
    var language = window.navigator.userLanguage || window.navigator.language;
    language = language.substring(0, 2);

    // supported language, temporary
    var allowedLanguages = ['pl'];

    for (var i = 0; i < allowedLanguages.length; i++) {
        if (allowedLanguages[i] == language)
            return language;
    }

    // default to first supported
    return allowedLanguages[0];
};

RegexEmbed.constructRequestUri = function(language, expression) {
    var str = "https://sds3.cs.put.poznan.pl/api/explainer/explain/";

    str = str + language + ".json?expression=" + encodeURIComponent(expression);

    return str;
};

RegexEmbed.asyncGet = function(uri, handler) {
    var request = new XMLHttpRequest();

    request.open('GET', uri, true);

    request.onreadystatechange = function (evt) {
        if (request.readyState == 4 && request.status == 200) {
            handler(JSON.parse(request.responseText));
        }
    };

    request.send(null);
};

RegexEmbed.handleElement = function(element, data, text) {
    var target = null;

    if (!data.target || data.target == 'this') {
        target = element;
    } else if (data.target.charAt(0) == '#') {
        target = document.getElementById(data.target.substring(1));
    }

    if (!data.handler || data.handler == 'title') {
        target.setAttribute('title', text);
    } else {
        target.textContent = text;
    }
};

RegexEmbed.loadData = function(element) {
    return {
        expression: element.getAttribute('data-regex2nl-expression') || '',
        target: element.getAttribute('data-regex2nl-target') || 'this',
        handler: element.getAttribute('data-regex2nl-handler') || 'title'
    };
};

RegexEmbed.explainAll = function() {
    var all = document.getElementsByTagName('*');

    for (var i = 0; i < all.length; i++) {
        if (all[i].hasAttribute('data-regex2nl-expression')) {
            this.explainElement(all[i]);
        }
    }
};

RegexEmbed.explainElement = function(element) {
    var data = this.loadData(element);

    if (data.expression == null || data.expression == '') {
        return;
    }

    var uri = this.constructRequestUri(this.guessLanguage(), data.expression);
    var that = this;

    this.asyncGet(uri, function (response) {
        that.handleElement(element, data, response.output);
    });
};

RegexEmbed.registerEvents = function() {
    var that = this;

    if (window.addEventListener) {
        window.addEventListener('DOMContentLoaded', function (ev) {
            that.explainAll();
        });
    } else if (window.attachEvent) {
        window.attachEvent('onreadystatechange', function (ev) {
            that.explainAll();
        });
    } else {
        window.onload = that.explainAll;
    }
};

RegexEmbed.registerEvents();
