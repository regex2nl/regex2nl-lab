%{
    #include "y.tab.h"
    #include <stdlib.h>
    void yyerror(char *);
typedef struct Node{
    int attr;
    char * name;
    struct Node * child;
    struct Node * brother;
    int alternativeId;
    int isRepetition;
    int index;
    char * value;
}Node;
typedef struct YYNodeVal {
    struct Node * first;
    struct Node * last;
    }YYNodeVal;
%}

%%

[0-9]      {   Node * node = malloc(sizeof(Node));
                node->alternativeId = 0;
                node->isRepetition = 0;
                node->name = malloc(sizeof("DIGIT"));
                strcpy(node->name, "DIGIT");
                node->value = malloc(sizeof(yytext));
                strcpy(node->value, yytext);
                YYNodeVal * val = malloc(sizeof(val));
                val->first = node;
                val->last = node;
                yylval.val = val;
                return DIGIT;
            }
[a-zA-Z<>\\]      {
                Node * node = malloc(sizeof(Node));
                node->alternativeId = 0;
                node->isRepetition = 0;
                node->name = malloc(sizeof("LETTER"));
                strcpy(node->name, "LETTER");
                node->value = malloc(sizeof(yytext));
                strcpy(node->value, yytext);
                YYNodeVal * val = malloc(sizeof(val));
                val->first = node;
                val->last = node;
                yylval.val = val;
                return LETTER;
            }

[+*|\(\)\[\]?\^,{}-]        {
                Node * node = malloc(sizeof(Node));
                node->alternativeId = 0;
                node->isRepetition = 0;
                node->name = malloc(sizeof(yytext));
                strcpy(node->name, yytext);
                YYNodeVal * val = malloc(sizeof(val));
                val->first = node;
                val->last = node;
                yylval.val = val;
                return yytext[0];
         }
\\.        {
                Node * node = malloc(sizeof(Node));
                node->alternativeId = 0;
                node->isRepetition = 0;
                node->name = malloc(sizeof("CHARACTER"));
                strcpy(node->name, "CHARACTER");
                node->value = malloc(sizeof(yytext));
                strcpy(node->value, yytext);
                YYNodeVal * val = malloc(sizeof(val));
                val->first = node;
                val->last = node;
                yylval.val = val;
                return CHARACTER;
         }
[ \t\n]     {;} /* skip whitespace */

.           {yyerror("invalid character ");yyerror(yytext);}

%%

int yywrap(void) {
    return 1;
}
