%{
#define MaxTxtLeng 10000 
#define MaxOptions 20
#define PREFIX "_CCC"
#define FUNC_PREFIX "_F_"
#include <string.h>
#include <stdio.h>
typedef enum ProdStatus {
     PROD_PREFIX = 0,
     PROD_BODY   = 1,
     PROD_POSTIX = 2 
     }ProdStatus;
void yyerror (char const *s);

void AddSimpleFunction(char * dest,char * string);
void FuncNameStore(char * dest,char * string, ProdStatus status,int index,int TermInnerCounter);
void LoopPrefix(char * dest,int TermInnerCounter);
void LoopClose(char * dest,int TermInnerCounter);
void FuncTextStore(char * dest,char * string);
void FuncAltInit(char * dest, int OptionsId);
void FuncAltEnd(char * dest);

void RuleEnd(char * options, char * functions);
void RuleInit(char * options, char * functions, char * functionsHeader, char * string);
void AddAction(char * options,int OptionsId);
void Store(int nTermId,int OptionsId,char * string,int index);
void StoreChar(int nTermId,int OptionsId,char * string,int index);
void ProdPrefix(int RepetitionAllCount, int nTermId,int OptionsId);
void ProdClose(char * repetitions, int TermInnerCount,int OptionsId);
void ProdPrint(int count,int OptionsCount);
void ProdInit(int OptionsId);
extern  char BoText[], BoName[],BoChar[],BoNumber;
int RepetitionLocalCounter = 0; //the number of repetitions non TerminalCounter occurrences in one non TerminalCounter
int RepetitionAllCounter = 0;   //the number of repetitions non TerminalCounter occurrences in whole program
int ProductionsCounter = 0;     //the number of no repetitions non TerminalCounter occurrences in whole program
int OptionsCounter;             //the number of OptionsCounter occurrences in one non TerminalCounter
int TermInnerCounter;           //the number of TerminalCounter and non TerminalCounter occurrences in one repetition
int TermOuterCounter;           //the number of TerminalCounter and non TerminalCounter occurrences in one no repetitions non TerminalCounter
int j;                          //loop counter  
ProdStatus prodStatus= PROD_PREFIX ;              //state in production 0 - prefix, 1- repetition, 2 - postfix
int RepetitionPosition= -1;         //position repetitions non TerminalCounter in production
int TerminalCounter=0;          //the number of TerminalCounter in %token section
char Repetitions[MaxOptions][MaxTxtLeng];
char OptionsArray[MaxOptions][MaxTxtLeng];
char FunctionsArray[MaxOptions][MaxTxtLeng];
char FunctionsHeaderArray[MaxOptions][MaxTxtLeng];
char FunctionFirst[MaxTxtLeng];
char FunctionsTerminalArray[MaxOptions][MaxTxtLeng];
char RuleName[MaxTxtLeng];
int Indexes[2][MaxOptions];

char buffer[300];
char yystype[] = 
"%union {\n"
"    struct YYNodeVal *val;\n"
"    };\n";
char structures[] =
"typedef struct Node{\n"
"    int attr;\n"
"    char * name;\n"
"    struct Node * child;\n"
"    struct Node * brother;\n"
"    int OptionsId;\n"
"    int status;\n"
"    int index;\n"
"    char * value;\n"
"    }Node;\n"
"typedef struct YYNodeVal {\n"
"    struct Node * first;\n"
"    struct Node * last;\n"
"    }YYNodeVal;\n";
char define[] =
"#include <string.h>\n"
"#include <stdio.h>\n"
"#include <sys/types.h>\n"
"#include <sys/stat.h>\n"
"#include <fcntl.h>\n"
"#include <unistd.h>\n"
"#define MaxTxtLeng 1000 \n"
"void yyerror (char const *s);\n";
char variables[] =
"Node * root;\n"
"int ident=0,i;\n"
"";
char function[] =
"void yyerror (char const *s) {\n"
"     fprintf (stderr, \"%s\\n\", s); \n"
"     }\n"
"void PrintTree(Node * node,FILE* destFile) {\n"
"     for(i=0;i<ident;i++)\n"
"         fwrite(\"    \", 1, 4, destFile);\n"
"     fwrite(node->name , 1 , strlen(node->name) , destFile );\n"
"     fwrite(\"\\n\", 1, 1, destFile);\n"
"     if(node->child != NULL) {\n"
"         ident++;\n"
"         PrintTree(node->child,destFile);\n"
"         ident--;\n"
"         }\n"
"   \n"
"     if(node->brother!= NULL && node->brother != node)\n"
"          PrintTree(node->brother,destFile);\n"
"     }\n"
"void FreeTree(Node * node) {\n"
"     if(node->child != NULL) \n"
"         FreeTree(node->child);\n"
"     if(node->brother!= NULL && node->brother != node)\n"
"          FreeTree(node->brother);\n"
"     free(node);\n"
"     }\n";

%}
%token Name Charac Text AuxBeg Number
%%
Prog: AuxDecl '%' '%' Rules
    ;
AuxDecl: AuxBeg Names {printf("\n%%%%\n");     }
       ;
  Names: Names Name   { 
                        printf(" %s ",BoName); 
                        AddSimpleFunction(FunctionsTerminalArray[TerminalCounter],BoName);
                        TerminalCounter++; 
                      }
       |              { printf("%%token ");}
       ;
  Rules: Rules Rule   { ProdPrint(RepetitionLocalCounter,OptionsCounter);      RepetitionLocalCounter =0;}
       |
       ;
   Rule: Name          { OptionsCounter=0; 
                         RuleInit(OptionsArray[OptionsCounter],FunctionsArray[ProductionsCounter],FunctionsHeaderArray[ProductionsCounter],BoName);
                         RepetitionLocalCounter =0; 
                        }
         '=' Options ';'{ 
                          RuleEnd(OptionsArray[OptionsCounter],FunctionsArray[ProductionsCounter]);
                          ProductionsCounter++; 
                        }
       ;
Options: Options        {
                         OptionsCounter++;
                         ProdInit(OptionsCounter); 
                        }
        '|' Option
       | Option
       ;
 Option: Pattern ':' { FuncAltInit(FunctionsArray[ProductionsCounter],OptionsCounter); } BPlate 
                        {FuncAltEnd(FunctionsArray[ProductionsCounter]); AddAction(OptionsArray[OptionsCounter],OptionsCounter);}
       ;
Pattern:  Pattern Item 
       |  Item     
       ;
   Item: Name            { Store(RepetitionLocalCounter,OptionsCounter,BoName,0);}
       | Name '^' Number { if(prodStatus == PROD_BODY) 
                               yyerror("Syntax error 1\n"); 
                           Store(RepetitionLocalCounter,OptionsCounter,BoName,BoNumber);
                         }      
       | Name '*'        { if(prodStatus != PROD_BODY) 
                               yyerror("Syntax error 3\n"); 
                           Store(RepetitionLocalCounter,OptionsCounter,BoName,0);
                         }
       | Name '*' Number { if(prodStatus != PROD_BODY) 
                               yyerror("Syntax error 4\n"); 
                           Store(RepetitionLocalCounter,OptionsCounter,BoName,BoNumber);
                         }         
       | Charac      { StoreChar(RepetitionLocalCounter,OptionsCounter,BoChar,-1);}
       | '{'         { ProdPrefix(RepetitionAllCounter,RepetitionLocalCounter,OptionsCounter); }
         Pattern '}' { 
                       ProdClose(Repetitions[RepetitionLocalCounter],TermInnerCounter,OptionsCounter); 
                       RepetitionLocalCounter++;
                       RepetitionAllCounter++;
                     }
       ;
 BPlate: BPlate Elem 
       |   Elem 
       ;
   Elem:    Text            { FuncTextStore(FunctionsArray[ProductionsCounter],BoText);            }  
       |    Name            { FuncNameStore(FunctionsArray[ProductionsCounter],BoName,0,-1,TermInnerCounter-1);       }
       |    Name '^' Number { FuncNameStore(FunctionsArray[ProductionsCounter],BoName,0,BoNumber,TermInnerCounter-1); }
       |    Name '*'        { FuncNameStore(FunctionsArray[ProductionsCounter],BoName,1,-1,TermInnerCounter-1);       } 
       |    Name '*' Number { FuncNameStore(FunctionsArray[ProductionsCounter],BoName,1,BoNumber,TermInnerCounter-1); } 
       |    Charac          { FuncTextStore(FunctionsArray[ProductionsCounter],BoChar);            } 
       |    '{'             { LoopPrefix(FunctionsArray[ProductionsCounter],TermInnerCounter-1);          }
            BPlate '}'      { LoopClose(FunctionsArray[ProductionsCounter],TermInnerCounter-1);                               }
       ;
%%
//Adds simpel function from %token witch return value of node.
void AddSimpleFunction(char * dest,char * string) {
     sprintf(dest,"char* %s%s(Node * node,int OptionsCounter) {\n",FUNC_PREFIX,string);
     strcat(dest,"     return node->value;\n");
     strcat(dest,"     }\n");
     }
//Adds to function code for one non TerminalCounter
void FuncNameStore(char * dest,char * string, ProdStatus status,int index,int TermInnerCounter){//FunctionsArray[ProductionsCounter]
     if(prodStatus != PROD_BODY && status != PROD_BODY){
         strcat(dest,"        child = node->child;\n");
         strcat(dest,"        while(child!= NULL){\n");
         if(index > 0)
             sprintf(buffer,"            if(strcmp(child->name,\"%s\")==0 && child->status == %d  && child->index ==%d){\n",string,status,index);
         else
             sprintf(buffer,"            if(strcmp(child->name,\"%s\")==0 && child->status == %d){\n",string,status);
         strcat(dest,buffer);
         sprintf(buffer,"                 strcat(result,%s%s(child,child->OptionsId));\n",FUNC_PREFIX,string);
         strcat(dest,buffer);
         strcat(dest,"                 break;\n");
         strcat(dest,"                 }\n");
         strcat(dest,"            child = child->brother;\n");
         strcat(dest,"            }\n"); 
         }   
     else if (prodStatus == PROD_BODY){
         sprintf(buffer,"				for(i=0;i<%d;i++){\n",TermInnerCounter);
         strcat(dest,buffer);
         if(index > 0)
             sprintf(buffer,"				    if(strcmp(nodeFrame[i]->name,\"%s\")==0 && nodeFrame[i]->index ==%d){\n",string,index);
         else
             sprintf(buffer,"				    if(strcmp(nodeFrame[i]->name,\"%s\")==0){\n",string);
         strcat(dest,buffer);
         sprintf(buffer,"				        strcat(result,%s%s(nodeFrame[i],nodeFrame[i]->OptionsId));\n",FUNC_PREFIX,string);
         strcat(dest,buffer);
         strcat(dest,"                        break;\n");
         strcat(dest,"				        }\n");
         strcat(dest,"				    }\n");
         }  
     else  
         yyerror("Syntax error need repetition\n");
     }
//Add to function initialize repetiotion from BPlate
void LoopPrefix(char * dest,int TermInnerCounter){
     if ( prodStatus )
         yyerror("Syntax error 5\n");
     prodStatus++;
     strcat(dest,"        child = node->child;\n");
     sprintf(buffer,"        Node* nodeFrame[%d];\n",TermInnerCounter);
     strcat(dest,buffer);
     strcat(dest,"        while(child!= NULL) {\n");
     strcat(dest,"            if(child->status ==1){\n");
     for(j=0;j<TermInnerCounter;j++) {
         sprintf(buffer,"                nodeFrame[%d] = child;\n",j);
         strcat(dest,buffer);
         if(j!=TermInnerCounter-1)
             strcat(dest,"                child = child->brother;\n");
         }
     }
//Add to function close of repetiotion from BPlate
void LoopClose(char * dest,int TermInnerCounter){
     prodStatus++;
     sprintf(buffer,"                child = nodeFrame[%d];\n",TermInnerCounter-1);
     strcat(dest,buffer);
     strcat(dest,"                }\n");
     strcat(dest,"            child = child->brother;\n");
     strcat(dest,"            }\n");
     }
//Add to function code for const string
void FuncTextStore(char * dest,char * string){
     if(prodStatus == PROD_BODY)
         sprintf(buffer,"                strcat(result,\"%s\");\n",string);
     else
         sprintf(buffer,"        strcat(result,\"%s\");\n",string);
     strcat(dest,buffer); 
     }
//Add to function initialize of single OptionsCounter
void FuncAltInit(char * dest, int OptionsId){
     if(OptionsId==0)
         sprintf(buffer,"    if(OptionsCounter == %d){\n",OptionsId);
     else
         sprintf(buffer,"    else if(OptionsCounter == %d){\n",OptionsId);
     strcat(dest, buffer); 
     prodStatus = PROD_PREFIX;     
     }
//Add to function end single OptionsCounter
void FuncAltEnd(char * dest){
     strcat(dest,"        }\n");
     }
//close action and function
void RuleEnd(char * options, char * functions) {
     strcat(options, "    ;\n");
     strcat(functions,"    return result;\n");  
     strcat(functions,"    }\n");  
     }
//initialize action and function
void RuleInit(char * options, char * functions, char * functionsHeader, char * string) {
     memset(options, 0, sizeof(options));
     strcpy(RuleName, string);
     sprintf(buffer,"char* %s%s(Node * node,int OptionsCounter) {\n",FUNC_PREFIX,RuleName);
     strcat( functions,buffer); 
     sprintf(buffer,"char* %s%s(Node * node,int OptionsCounter);\n",FUNC_PREFIX,RuleName);
     memcpy( functionsHeader,buffer,sizeof(buffer)); 

     strcat(functions,"    char * result = malloc(MaxTxtLeng);\n");
     strcat(functions,"    Node *child;\n");    
     }
//clear memory allocated to YYNodeVal
void FreeYYNodeVal(char * dest, int termCount, int repetitnionId){
     for(j=1;j<=termCount;j++) {
         if(repetitnionId == 0 && j==1 ) { 
             strcat(dest,"            if($<val>1 != $<val>2)\n    ");
             }
         else if(  repetitnionId ==j && repetitnionId >0) {
             sprintf(buffer,"            if($<val>%d != $<val>%d)\n    ",j,j+1);
             strcat(dest,buffer); 
             }   
         sprintf(buffer,"            free($<val>%d);\n",j);
         strcat(dest,buffer);   
         }
     }
//Create a list of brothers
void MakeList(char * dest, int termCount, int repetitnionId){
     if(repetitnionId == 0) {
         strcat(dest,"            if($<val>1 != NULL) \n"); 
         if(termCount > 1)
             strcat(dest,"                $<val>1->last->brother = $<val>2->first;\n"); 
         strcat(dest,"            else\n");
         strcat(dest,"                $<val>1 = $<val>2;\n");   
         }

     for(j=1;j<termCount;j++) {
         if(repetitnionId == j ) {
             sprintf(buffer,"            if($<val>%d != NULL)\n",j+1);
             strcat(dest,buffer);
             sprintf(buffer,"                $<val>%d->last->brother = $<val>%d->first;\n",j,j+1);
             strcat(dest,buffer);
             strcat(dest,"            else\n");
             sprintf(buffer,"                $<val>%d = $<val>%d;\n",j+1,j);
             strcat(dest,buffer);
             }
         else if(repetitnionId != 0 || j != 1){
             sprintf(buffer,"            $<val>%d->last->brother = $<val>%d->first;\n",j,j+1);
             strcat(dest,buffer); 

             }
         }
     }
void SetIndex(char * dest,int id) {
     if(Indexes[0][id-1] !=0) {
         sprintf(buffer,"            $<val>%d->first->index =%d;\n",id,Indexes[0][id-1]);
         strcat(dest,buffer); 
         }
     else if(Indexes[0][id-1] ==0) {
         sprintf(buffer,"            $<val>%d->first->index =%d;\n",id,id);
         strcat(dest,buffer); 
         }
     }
void SetIndexes(char * dest,int termCount, int repetitnionId) {
     for(j=1;j<=TermOuterCounter;j++) {
	     if(j<repetitnionId+1 || repetitnionId ==-1){
             SetIndex(dest,j);
             }
	     else if(j> repetitnionId+1 && repetitnionId>=0){
             SetIndex(dest,j); 
             }
         }
     }
void SetStatuses(char * dest,int termCount, int repetitnionId){
     for(j=1;j<=TermOuterCounter;j++) {
	     if(j<repetitnionId+1 || repetitnionId ==-1){
             sprintf(buffer,"            $<val>%d->first->status =0;\n",j);
             strcat(dest,buffer);
             }
	     else if(j> repetitnionId+1 && repetitnionId>=0){
             sprintf(buffer,"            $<val>%d->first->status =0;\n",j);
             strcat(dest,buffer);  
             }
         }
     }
void AddAction(char * options,int OptionsId) {
     strcat(options,"\n        {\n");   
     strcat(options,"            Node * node = malloc(sizeof(Node));\n"); 
     sprintf(buffer,"            node->OptionsId = %d;\n",OptionsId);
     strcat(options,buffer);
     strcat(options,"            node->status = 0;\n");    
     sprintf(buffer,"            node->name = malloc(sizeof(\"%s\"));\n",RuleName);
     strcat(options,buffer);  
     sprintf(buffer,"            strcpy(node->name, \"%s\");\n",RuleName);
     strcat(options,buffer); 

     SetStatuses(options,TermOuterCounter,RepetitionPosition);
     strcat(options,"\n"); 

     SetIndexes(options,TermOuterCounter,RepetitionPosition);
     strcat(options,"\n"); 

     MakeList(options,TermOuterCounter,RepetitionPosition);
     strcat(options,"            node->child = $<val>1->first;\n");
     strcat(options,"\n"); 

     FreeYYNodeVal(options,TermOuterCounter,RepetitionPosition);
     strcat(options,"\n"); 

     if(ProductionsCounter==0) {
         strcat(options,"            root = node;\n"); 	
         memcpy( FunctionFirst,RuleName,sizeof(RuleName)); 
	     }
     else {
         strcat(options,"            $<val>$ = malloc(sizeof(YYNodeVal));\n");
         strcat(options,"            $<val>$->first = node;\n");	
         strcat(options,"            $<val>$->last = node;\n");	
         }
     strcat(options,"        }\n");	
     TermOuterCounter =0;
     RepetitionPosition = -1;
     }
void StoreChar(int nTermId,int OptionsId,char * string,int index) {
     char charBuffor[4];
     sprintf(charBuffor,"\'%s\'",string);
     Store(nTermId,OptionsId,charBuffor,index);
     }
void Store(int nTermId,int OptionsId,char * string,int index) {
     if ( prodStatus != PROD_BODY) {
          strcat(OptionsArray[OptionsId],string);
          strcat(OptionsArray[OptionsId]," ");
          Indexes[0][TermOuterCounter] = index;
          TermOuterCounter++;
          }
     else {
          strcat(Repetitions[nTermId],string);
          strcat(Repetitions[nTermId]," ");  
          Indexes[1][TermInnerCounter] = index;
          TermInnerCounter++;
          }

     }
void ProdPrefix(int RepetitionAllCount, int nTermId,int OptionsId) {
     RepetitionPosition = TermOuterCounter;
     TermOuterCounter++;
     TermInnerCounter = 1;
     if ( prodStatus )
         yyerror("Syntax error 6\n");
     prodStatus ++;
     sprintf(buffer,"%s%d ",PREFIX,RepetitionAllCount);
     strcat(OptionsArray[OptionsId],buffer); 
     memset(Repetitions[nTermId], 0, sizeof(Repetitions[nTermId]));
     sprintf(Repetitions[nTermId], "%s%d: %s%d ",PREFIX,RepetitionAllCount,PREFIX,RepetitionAllCount); 
     }
void ProdClose(char * repetitions, int TermInnerCount,int OptionsId) {
     prodStatus ++;
     strcat(repetitions,"\n");
     strcat(repetitions,"\n        {\n");

     
     for(j=1;j<TermInnerCount;j++)
     {    
         if(j==1) {
             strcat(repetitions,"            if($<val>1== NULL)\n");
             strcat(repetitions,"                $<val>1 = $<val>2;\n");
             strcat(repetitions,"            else \n");
             sprintf(buffer,"                $<val>%d->last->brother = $<val>%d->first;\n",j,j+1);
             strcat(repetitions,buffer);

             }
         else{
             sprintf(buffer,"            $<val>%d->first->brother = $<val>%d->first;\n",j,j+1);
             strcat(repetitions,buffer);
             }
         }
     strcat(repetitions,"\n");
     for(j=1;j<=TermInnerCount;j++) {
         sprintf(buffer,"            $<val>%d->first->status = 1;\n",j);
         strcat(repetitions,buffer);
         }
     strcat(repetitions,"\n");

     for(j=1;j<TermInnerCount;j++) {
         if(Indexes[1][j] !=0){
             sprintf(buffer,"            $<val>%d->first->index = %d;\n",j+1,Indexes[1][j]);
             strcat(repetitions,buffer);
             }
         else if(Indexes[1][j] ==0){
             sprintf(buffer,"            $<val>%d->first->index = %d;\n",j+1,j);
             strcat(repetitions,buffer);
             }
         }
     strcat(repetitions,"\n");
     sprintf(buffer,"            $<val>1->last = $<val>%d->last;\n",TermInnerCount);
     strcat(repetitions,buffer);

     strcat(repetitions,"            if($<val>1 != $<val>2)\n");
     strcat(repetitions,"                free($<val>2);\n");
     for(j=2;j<TermInnerCount;j++) {
         sprintf(buffer,"            free($<val>%d);\n",j+1);
         strcat(repetitions,buffer);
         }

     strcat(repetitions,"            $<val>$ = $<val>1;\n");
	 strcat(repetitions,"        }\n");	

     strcat(repetitions,"\t|\n");
     strcat(repetitions,"        {\n");
     strcat(repetitions,"            $<val>$ = NULL;\n");
	 strcat(repetitions,"        }\n");
     strcat(repetitions,"    ;");
     }
void ProdPrint(int count,int OptionsCount) {
     printf("%s: ",RuleName);
     for(j=0; j<=OptionsCount; j++){
         if(j>0){
             printf("    | "); 
             }
         printf("%s",OptionsArray[j]);
         }
     for(j=0; j<count; j++)
         printf("%s\n",Repetitions[j]);
     }
void ProdInit(int OptionsId) {
     prodStatus = PROD_PREFIX;
     memset(OptionsArray[OptionsId], 0, sizeof(OptionsArray[OptionsId]));  
     }
void yyerror (char const *s) {
     fprintf (stderr, "%s\n", s); 
     }
 int main() {
     printf("%%{\n");
     printf("%s\n",define);
     printf("%s\n",structures);
     printf("%s\n",variables);
     printf("%%}\n");
     printf("%s\n",yystype); 
     yyparse();
     printf("%%%%\n");
     for(j=0;j<=ProductionsCounter;j++)
         printf("%s",FunctionsHeaderArray[j]);
     printf("\n"); 
     for(j=0;j<=TerminalCounter;j++)
         printf("%s",FunctionsTerminalArray[j]);
     for(j=0;j<=ProductionsCounter;j++)
         printf("%s",FunctionsArray[j]);
     printf("%s\n",function);
     printf(" int main() {\n"
"     yyparse();\n"
"     FILE* destFile;\n"
"     destFile = fopen(\"tree.txt\", \"wb\");\n"
"     PrintTree(root,destFile);\n"
"     printf(\"\\n\");\n"
"     printf(\"%%s\",%s%s(root,root->OptionsId));\n"
"     fclose(destFile);\n"
"     FreeTree(root);\n"
"     return 0;\n"
"     }\n",FUNC_PREFIX,FunctionFirst);

     return 0;
     }
