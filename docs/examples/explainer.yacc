%{
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define MaxTxtLeng 1000 
void yyerror (char const *s);

typedef struct Node{
    int attr;
    char * name;
    struct Node * child;
    struct Node * brother;
    int OptionsId;
    int status;
    int index;
    char * value;
    }Node;
typedef struct YYNodeVal {
    struct Node * first;
    struct Node * last;
    }YYNodeVal;

Node * root;
int ident=0,i;

%}
%union {
    struct YYNodeVal *val;
    };

%token  LETTER  DIGIT  CHARACTER 
%%
Expr: Regex 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Expr"));
            strcpy(node->name, "Expr");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            root = node;
        }
    ;
Regex: Comp _CCC0 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Regex"));
            strcpy(node->name, "Regex");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            if($<val>2 != NULL)
                $<val>1->last->brother = $<val>2->first;
            else
                $<val>2 = $<val>1;
            node->child = $<val>1->first;

            if($<val>1 != $<val>2)
                free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
_CCC0: _CCC0 '|' Comp 

        {
            if($<val>1== NULL)
                $<val>1 = $<val>2;
            else 
                $<val>1->last->brother = $<val>2->first;
            $<val>2->first->brother = $<val>3->first;

            $<val>1->first->status = 1;
            $<val>2->first->status = 1;
            $<val>3->first->status = 1;

            $<val>2->first->index = -1;
            $<val>3->first->index = 2;

            $<val>1->last = $<val>3->last;
            if($<val>1 != $<val>2)
                free($<val>2);
            free($<val>3);
            $<val>$ = $<val>1;
        }
	|
        {
            $<val>$ = NULL;
        }
    ;
Comp: Fact 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Comp"));
            strcpy(node->name, "Comp");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Fact _CCC1 Fact 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 1;
            node->status = 0;
            node->name = malloc(sizeof("Comp"));
            strcpy(node->name, "Comp");
            $<val>1->first->status =0;
            $<val>3->first->status =0;

            $<val>1->first->index =1;
            $<val>3->first->index =2;

            if($<val>2 != NULL)
                $<val>1->last->brother = $<val>2->first;
            else
                $<val>2 = $<val>1;
            $<val>2->last->brother = $<val>3->first;
            node->child = $<val>1->first;

            if($<val>1 != $<val>2)
                free($<val>1);
            free($<val>2);
            free($<val>3);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
_CCC1: _CCC1 Fact 

        {
            if($<val>1== NULL)
                $<val>1 = $<val>2;
            else 
                $<val>1->last->brother = $<val>2->first;

            $<val>1->first->status = 1;
            $<val>2->first->status = 1;

            $<val>2->first->index = 1;

            $<val>1->last = $<val>2->last;
            if($<val>1 != $<val>2)
                free($<val>2);
            $<val>$ = $<val>1;
        }
	|
        {
            $<val>$ = NULL;
        }
    ;
Fact: Group '+' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Fact"));
            strcpy(node->name, "Fact");
            $<val>1->first->status =0;
            $<val>2->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Group '*' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 1;
            node->status = 0;
            node->name = malloc(sizeof("Fact"));
            strcpy(node->name, "Fact");
            $<val>1->first->status =0;
            $<val>2->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Group '?' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 2;
            node->status = 0;
            node->name = malloc(sizeof("Fact"));
            strcpy(node->name, "Fact");
            $<val>1->first->status =0;
            $<val>2->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Group 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 3;
            node->status = 0;
            node->name = malloc(sizeof("Fact"));
            strcpy(node->name, "Fact");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
Group: Prim 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Group"));
            strcpy(node->name, "Group");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Prim '{' Amount '}' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 1;
            node->status = 0;
            node->name = malloc(sizeof("Group"));
            strcpy(node->name, "Group");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;
            $<val>4->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =1;
            $<val>4->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            $<val>3->last->brother = $<val>4->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);
            free($<val>4);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Prim '{' ',' Amount '}' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 2;
            node->status = 0;
            node->name = malloc(sizeof("Group"));
            strcpy(node->name, "Group");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;
            $<val>4->first->status =0;
            $<val>5->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =-1;
            $<val>4->first->index =1;
            $<val>5->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            $<val>3->last->brother = $<val>4->first;
            $<val>4->last->brother = $<val>5->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);
            free($<val>4);
            free($<val>5);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Prim '{' Amount ',' '}' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 3;
            node->status = 0;
            node->name = malloc(sizeof("Group"));
            strcpy(node->name, "Group");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;
            $<val>4->first->status =0;
            $<val>5->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =1;
            $<val>4->first->index =-1;
            $<val>5->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            $<val>3->last->brother = $<val>4->first;
            $<val>4->last->brother = $<val>5->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);
            free($<val>4);
            free($<val>5);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | Prim '{' Amount ',' Amount '}' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 4;
            node->status = 0;
            node->name = malloc(sizeof("Group"));
            strcpy(node->name, "Group");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;
            $<val>4->first->status =0;
            $<val>5->first->status =0;
            $<val>6->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =1;
            $<val>4->first->index =-1;
            $<val>5->first->index =2;
            $<val>6->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            $<val>3->last->brother = $<val>4->first;
            $<val>4->last->brother = $<val>5->first;
            $<val>5->last->brother = $<val>6->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);
            free($<val>4);
            free($<val>5);
            free($<val>6);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
Amount: DIGIT _CCC2 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Amount"));
            strcpy(node->name, "Amount");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            if($<val>2 != NULL)
                $<val>1->last->brother = $<val>2->first;
            else
                $<val>2 = $<val>1;
            node->child = $<val>1->first;

            if($<val>1 != $<val>2)
                free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
_CCC2: _CCC2 DIGIT 

        {
            if($<val>1== NULL)
                $<val>1 = $<val>2;
            else 
                $<val>1->last->brother = $<val>2->first;

            $<val>1->first->status = 1;
            $<val>2->first->status = 1;

            $<val>2->first->index = 1;

            $<val>1->last = $<val>2->last;
            if($<val>1 != $<val>2)
                free($<val>2);
            $<val>$ = $<val>1;
        }
	|
        {
            $<val>$ = NULL;
        }
    ;
Prim: '(' Regex ')' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Prim"));
            strcpy(node->name, "Prim");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;

            $<val>1->first->index =-1;
            $<val>2->first->index =2;
            $<val>3->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | '[' Set ']' 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 1;
            node->status = 0;
            node->name = malloc(sizeof("Prim"));
            strcpy(node->name, "Prim");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;

            $<val>1->first->index =-1;
            $<val>2->first->index =2;
            $<val>3->first->index =-1;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | CHARACTER 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 2;
            node->status = 0;
            node->name = malloc(sizeof("Prim"));
            strcpy(node->name, "Prim");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | DIGIT 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 3;
            node->status = 0;
            node->name = malloc(sizeof("Prim"));
            strcpy(node->name, "Prim");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | LETTER 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 4;
            node->status = 0;
            node->name = malloc(sizeof("Prim"));
            strcpy(node->name, "Prim");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
Set: Item _CCC3 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Set"));
            strcpy(node->name, "Set");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            if($<val>2 != NULL)
                $<val>1->last->brother = $<val>2->first;
            else
                $<val>2 = $<val>1;
            node->child = $<val>1->first;

            if($<val>1 != $<val>2)
                free($<val>1);
            free($<val>2);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
_CCC3: _CCC3 Item 

        {
            if($<val>1== NULL)
                $<val>1 = $<val>2;
            else 
                $<val>1->last->brother = $<val>2->first;

            $<val>1->first->status = 1;
            $<val>2->first->status = 1;

            $<val>2->first->index = 1;

            $<val>1->last = $<val>2->last;
            if($<val>1 != $<val>2)
                free($<val>2);
            $<val>$ = $<val>1;
        }
	|
        {
            $<val>$ = NULL;
        }
    ;
Item: CHARACTER 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 0;
            node->status = 0;
            node->name = malloc(sizeof("Item"));
            strcpy(node->name, "Item");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | DIGIT 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 1;
            node->status = 0;
            node->name = malloc(sizeof("Item"));
            strcpy(node->name, "Item");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | LETTER 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 2;
            node->status = 0;
            node->name = malloc(sizeof("Item"));
            strcpy(node->name, "Item");
            $<val>1->first->status =0;

            $<val>1->first->index =1;

            node->child = $<val>1->first;

            free($<val>1);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | DIGIT '-' DIGIT 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 3;
            node->status = 0;
            node->name = malloc(sizeof("Item"));
            strcpy(node->name, "Item");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =2;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    | LETTER '-' LETTER 
        {
            Node * node = malloc(sizeof(Node));
            node->OptionsId = 4;
            node->status = 0;
            node->name = malloc(sizeof("Item"));
            strcpy(node->name, "Item");
            $<val>1->first->status =0;
            $<val>2->first->status =0;
            $<val>3->first->status =0;

            $<val>1->first->index =1;
            $<val>2->first->index =-1;
            $<val>3->first->index =2;

            $<val>1->last->brother = $<val>2->first;
            $<val>2->last->brother = $<val>3->first;
            node->child = $<val>1->first;

            free($<val>1);
            free($<val>2);
            free($<val>3);

            $<val>$ = malloc(sizeof(YYNodeVal));
            $<val>$->first = node;
            $<val>$->last = node;
        }
    ;
%%
char* _F_Expr(Node * node,int OptionsCounter);
char* _F_Regex(Node * node,int OptionsCounter);
char* _F_Comp(Node * node,int OptionsCounter);
char* _F_Fact(Node * node,int OptionsCounter);
char* _F_Group(Node * node,int OptionsCounter);
char* _F_Amount(Node * node,int OptionsCounter);
char* _F_Prim(Node * node,int OptionsCounter);
char* _F_Set(Node * node,int OptionsCounter);
char* _F_Item(Node * node,int OptionsCounter);

char* _F_LETTER(Node * node,int OptionsCounter) {
     return node->value;
     }
char* _F_DIGIT(Node * node,int OptionsCounter) {
     return node->value;
     }
char* _F_CHARACTER(Node * node,int OptionsCounter) {
     return node->value;
     }
char* _F_Expr(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        strcat(result,"It is ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Regex")==0 && child->status == 0){
                 strcat(result,_F_Regex(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result,". ");
        }
    return result;
    }
char* _F_Regex(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Comp")==0 && child->status == 0){
                 strcat(result,_F_Comp(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        child = node->child;
        Node* nodeFrame[2];
        while(child!= NULL) {
            if(child->status ==1){
                nodeFrame[0] = child;
                child = child->brother;
                nodeFrame[1] = child;
                strcat(result,"or ");
				for(i=0;i<2;i++){
				    if(strcmp(nodeFrame[i]->name,"Comp")==0){
				        strcat(result,_F_Comp(nodeFrame[i],nodeFrame[i]->OptionsId));
                        break;
				        }
				    }
                child = nodeFrame[1];
                }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Comp(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Fact")==0 && child->status == 0){
                 strcat(result,_F_Fact(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 1){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Fact")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_Fact(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        child = node->child;
        Node* nodeFrame[1];
        while(child!= NULL) {
            if(child->status ==1){
                nodeFrame[0] = child;
                strcat(result,", ");
				for(i=0;i<1;i++){
				    if(strcmp(nodeFrame[i]->name,"Fact")==0){
				        strcat(result,_F_Fact(nodeFrame[i],nodeFrame[i]->OptionsId));
                        break;
				        }
				    }
                child = nodeFrame[0];
                }
            child = child->brother;
            }
        strcat(result,"followed by ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Fact")==0 && child->status == 0  && child->index ==2){
                 strcat(result,_F_Fact(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Fact(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        strcat(result,"a nonempty sequence of ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Group")==0 && child->status == 0){
                 strcat(result,_F_Group(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 1){
        strcat(result,"a possibly empty sequence of ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Group")==0 && child->status == 0){
                 strcat(result,_F_Group(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 2){
        strcat(result,"an optional ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Group")==0 && child->status == 0){
                 strcat(result,_F_Group(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 3){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Group")==0 && child->status == 0){
                 strcat(result,_F_Group(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Group(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Prim")==0 && child->status == 0){
                 strcat(result,_F_Prim(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 1){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Prim")==0 && child->status == 0){
                 strcat(result,_F_Prim(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," exactly ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Amount")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_Amount(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," times ");
        }
    else if(OptionsCounter == 2){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Prim")==0 && child->status == 0){
                 strcat(result,_F_Prim(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," maximum ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Amount")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_Amount(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," times ");
        }
    else if(OptionsCounter == 3){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Prim")==0 && child->status == 0){
                 strcat(result,_F_Prim(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," minimum ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Amount")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_Amount(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," times ");
        }
    else if(OptionsCounter == 4){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Prim")==0 && child->status == 0){
                 strcat(result,_F_Prim(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," at least ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Amount")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_Amount(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," but not more than ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Amount")==0 && child->status == 0  && child->index ==2){
                 strcat(result,_F_Amount(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," times ");
        }
    return result;
    }
char* _F_Amount(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        child = node->child;
        Node* nodeFrame[1];
        while(child!= NULL) {
            if(child->status ==1){
                nodeFrame[0] = child;
				for(i=0;i<1;i++){
				    if(strcmp(nodeFrame[i]->name,"DIGIT")==0){
				        strcat(result,_F_DIGIT(nodeFrame[i],nodeFrame[i]->OptionsId));
                        break;
				        }
				    }
                child = nodeFrame[0];
                }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Prim(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Regex")==0 && child->status == 0){
                 strcat(result,_F_Regex(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 1){
        strcat(result,"any of the following characters:\n ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Set")==0 && child->status == 0){
                 strcat(result,_F_Set(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result,"\n ");
        }
    else if(OptionsCounter == 2){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"CHARACTER")==0 && child->status == 0){
                 strcat(result,_F_CHARACTER(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 3){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 4){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"LETTER")==0 && child->status == 0){
                 strcat(result,_F_LETTER(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Set(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"Item")==0 && child->status == 0){
                 strcat(result,_F_Item(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        child = node->child;
        Node* nodeFrame[1];
        while(child!= NULL) {
            if(child->status ==1){
                nodeFrame[0] = child;
                strcat(result,", ");
				for(i=0;i<1;i++){
				    if(strcmp(nodeFrame[i]->name,"Item")==0){
				        strcat(result,_F_Item(nodeFrame[i],nodeFrame[i]->OptionsId));
                        break;
				        }
				    }
                child = nodeFrame[0];
                }
            child = child->brother;
            }
        }
    return result;
    }
char* _F_Item(Node * node,int OptionsCounter) {
    char * result = malloc(MaxTxtLeng);
    Node *child;
    if(OptionsCounter == 0){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"CHARACTER")==0 && child->status == 0){
                 strcat(result,_F_CHARACTER(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 1){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 2){
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"LETTER")==0 && child->status == 0){
                 strcat(result,_F_LETTER(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 3){
        strcat(result,"any digit from ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," to ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0  && child->index ==2){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    else if(OptionsCounter == 4){
        strcat(result,"any letter from ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0  && child->index ==1){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        strcat(result," to ");
        child = node->child;
        while(child!= NULL){
            if(strcmp(child->name,"DIGIT")==0 && child->status == 0  && child->index ==2){
                 strcat(result,_F_DIGIT(child,child->OptionsId));
                 break;
                 }
            child = child->brother;
            }
        }
    return result;
    }
void yyerror (char const *s) {
     fprintf (stderr, "%s\n", s); 
     }
void PrintTree(Node * node,FILE* destFile) {
     for(i=0;i<ident;i++)
         fwrite("    ", 1, 4, destFile);
     fwrite(node->name , 1 , strlen(node->name) , destFile );
     fwrite("\n", 1, 1, destFile);
     if(node->child != NULL) {
         ident++;
         PrintTree(node->child,destFile);
         ident--;
         }
   
     if(node->brother!= NULL && node->brother != node)
          PrintTree(node->brother,destFile);
     }
void FreeTree(Node * node) {
     if(node->child != NULL) 
         FreeTree(node->child);
     if(node->brother!= NULL && node->brother != node)
          FreeTree(node->brother);
     free(node);
     }

 int main() {
     yyparse();
     FILE* destFile;
     destFile = fopen("tree.txt", "wb");
     PrintTree(root,destFile);
     printf("\n");
     printf("%s",_F_Expr(root,root->OptionsId));
     fclose(destFile);
     FreeTree(root);
     return 0;
     }
