%{
#define MaxTxtLeng 100  
#define MaxCharLeng 2
#include <string.h>
#include "y.tab.h"
char BoText[MaxTxtLeng], BoName[MaxTxtLeng],BoChar[MaxCharLeng],BoNumber;
void yyerror (char const *s);
%}
%%
[|=:;%{}\^\$\*]   { return yytext[0]; }
[A-Za-z]+   { memset(BoName, 0, sizeof(BoName));
              strncpy(BoName, &yytext[0], yyleng);
              BoName[yyleng]= '\0';
              return Name; }
\'.\'       { /*** if(Modify) see peek #1***/
              memset(BoChar, 0, sizeof(BoChar));
              sprintf(BoChar, "%c",yytext[1]);
              return Charac; }
\"[^"]*\"   { /*** Text ***/
              memset(BoText, 0, sizeof(BoText));
              strncpy(BoText, &yytext[1], yyleng-2);
              BoText[yyleng]= '\0';
              return Text; }
[0-9]+      { BoNumber =atoi(yytext);
              return Number; }
%token    {   return AuxBeg; }
[ \t\n]     { ; }
.           { printf("Error Char :'%s'",yytext);
              yyerror("Unknown char - Sorry!"); }
%%
int yywrap(void){
    return 1;
    }
/***Peeks ***/
/*** #1 Reprezentation of character is {c,\0} where c is character***/
