<?php
/**
 * Class FilesystemEnvironment
 * @package App\Compiler\Environment
 */


namespace Compiler\Environment;


use Compiler\CompilerException;
use Compiler\Environment;
use Ramsey\Uuid\Uuid;
use SplFileInfo;

class FilesystemEnvironment implements Environment
{
    protected $created = false;
    protected $fullPath = null;
    protected $name = null;

    public function __construct($storage, $name = null, $mode = 0777)
    {
        if (empty($name))
            $name = Uuid::uuid4()->toString();

        $this->name = $name;
        $this->fullPath = $storage.DIRECTORY_SEPARATOR.$name;

        if (!is_dir($this->fullPath)) {
            if (!$mode) {
                throw new CompilerException("Mode specified to be zero and directory doesn't already exist");
            }
        } else {
            $this->created = true;
        }
    }

    private function ensureExists()
    {
        if ($this->created)
            return;

        if (!mkdir($this->fullPath)) {
            throw new CompilerException("Directory could not be created for: ".$this->fullPath);
        }

        $this->created = true;
    }

    public function copy($copy, $getFilepath)
    {
        copy($getFilepath, $this->getFilepath($copy));
    }

    public function generateFile($prefix, $extension)
    {
        if (!is_file($this->getFilepath($prefix.'.'.$extension)))
            return $prefix.'.'.$extension;

        $counter = 2;

        while (is_file($this->getFilepath($prefix.'-'.$counter.'.'.$extension)))
            $counter++;

        return $prefix.'-'.$counter.'.'.$extension;
    }

    public function save($copy, $getString)
    {
        file_put_contents($this->getFilepath($copy), $getString);
    }

    public function getFilepath($copy)
    {
        $this->ensureExists();

        return $this->fullPath.DIRECTORY_SEPARATOR.$copy;
    }

    public function getName()
    {
        return $this->name;
    }

    public function read($file)
    {
        return file_get_contents($this->getFilepath($file));
    }

    public function getWorkingDirectory()
    {
        $this->ensureExists();

        return $this->fullPath;
    }

    public function contains($file)
    {
        return is_file($this->getFilepath($file));
    }

    public function equals(Environment $environment)
    {
        if ($environment instanceof FilesystemEnvironment) {
            return $this->fullPath == $environment->getWorkingDirectory() &&
                $this->name == $environment->getName();
        }

        return false;
    }

    public function dispose()
    {
        if (!$this->created)
            return;

        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(
            $this->getWorkingDirectory(), \RecursiveDirectoryIterator::SKIP_DOTS
        ), \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $f) {
            if ($f->isDir()) {
                @rmdir($f->getRealPath());
            } else {
                @unlink($f->getRealPath());
            }
        }

        @rmdir($this->getWorkingDirectory());
    }
}
