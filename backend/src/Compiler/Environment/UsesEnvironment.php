<?php
/**
 * Class UsesEnvironment
 * @package App\Compiler\Element
 */


namespace Compiler\Environment;


use Compiler\Environment;

interface UsesEnvironment
{
    public function getEnvironment() : Environment;
}
