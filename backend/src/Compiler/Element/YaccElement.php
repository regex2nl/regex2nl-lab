<?php
/**
 * Class YaccElement
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\Data;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Symfony\Component\Process\Process;

class YaccElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;
    use Element\Generic\ProcessExecution;
    use Element\Generic\StoresEnvironment;
    use Element\Generic\LazyDrain;

    public function __construct(Environment $environment, InfoStream $infoStream)
    {
        $this->environment = $environment;
        $this->infoStream = $infoStream;
    }

    public function fetch() : array
    {
        $this->execute();

        $reference = new Data\EnvironmentReferenceData('y.tab.c', $this->environment);
        $referenceh = new Data\EnvironmentReferenceData('y.tab.h', $this->environment);

        $this->infoStream->push(new InfoStream\ArtifactInfo($reference));
        $this->infoStream->push(new InfoStream\ArtifactInfo($referenceh));

        return [
            'c-source' => $reference
        ];
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('source'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('c-source'));
    }

    protected function createProcess() : Process
    {
        $source = $this->downloadSource('source')->getFilepath();

        $process = new Process('/usr/bin/yacc -d '.escapeshellarg($source));
        $process->setWorkingDirectory($this->environment->getWorkingDirectory());

        return $process;
    }
}
