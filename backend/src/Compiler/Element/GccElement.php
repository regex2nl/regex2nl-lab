<?php
/**
 * Class GccElement
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\Data\EnvironmentReferenceData;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Symfony\Component\Process\Process;

class GccElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;
    use Element\Generic\ProcessExecution;
    use Element\Generic\StoresEnvironment;
    use Element\Generic\LazyDrain;

    protected $include;
    protected $output;

    public function __construct(Environment $environment, InfoStream $infoStream, $output = 'application', $include = null)
    {
        $this->include = $include;
        $this->environment = $environment;
        $this->output = $output;
        $this->infoStream = $infoStream;
    }

    public function fetch() : array
    {
        $this->execute();

        return [
            'exec' => new Data\EnvironmentReferenceData($this->output, $this->environment)
        ];
    }

    public function accepts(string $name) : bool
    {
        return substr($name, 0, 7) == 'source-';
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('exec'));
    }

    protected function createProcess() : Process
    {
        $sources = $this->downloadAllSourcesStartingWith('source-');

        if (empty($sources))
            throw new CompilerException("No sources provided for GCC");

        $command = '/usr/bin/gcc ';

        if (!empty($this->include))
            $command .= '-I'.escapeshellarg($this->include).' ';

        foreach ($sources as $s) {
            $command .= escapeshellarg($s->getFilepath()).' ';
        }

        $command .= '-o '.$this->environment->getFilepath($this->output);

        $process = new Process($command);
        $process->setWorkingDirectory($this->environment->getWorkingDirectory());

        return $process;
    }
}
