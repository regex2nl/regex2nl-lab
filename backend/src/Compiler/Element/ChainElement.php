<?php
/**
 * Class ChainElement
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Compiler\Pipe;
use Symfony\Component\Process\Process;

abstract class ChainElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;
    use Element\Generic\StoresEnvironment;

    protected $children = array();
    protected $sinkMapping = array();

    public function __construct(Environment $environment, InfoStream $infoStream)
    {
        $this->environment = $environment;
        $this->infoStream = $infoStream;
    }

    public function drain(string $name) : Data
    {
        if (isset($this->sinkMapping[$name]))
            return $this->sinkMapping[$name]->drain();

        throw new CompilerException("Cannot drain ".$name." from chain");
    }

    public function chainDrain(string $name)
    {
        if (isset($this->sources[$name]))
            return $this->sources[$name]->drain();

        return null;
    }

    protected function chainPipe(string $name, $original = null) : Pipe
    {
        return new Pipe\ChainPipe($this, $name, $original);
    }

    protected function wrapPipe(Pipe $pipe) : Pipe
    {
        return new Pipe\EnvironmentWrapperPipe($pipe, $this->environment);
    }

    protected function addChild($id, Element $element, array $mapSource = [], array $mapSink = [])
    {
        foreach ($mapSource as $k => $v)
            $element->connect($k, $this->chainPipe($v));

        foreach ($mapSink as $k => $v)
            $this->sinkMapping[$k] = $element->pipe($v);

        $this->children[$id] = $element;
    }
}
