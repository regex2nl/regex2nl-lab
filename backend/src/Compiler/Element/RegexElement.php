<?php
/**
 * Class RegexElement
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\Data\EnvironmentReferenceData;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Symfony\Component\Process\Process;

class RegexElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;

    use Element\Generic\StdoutCollector, Element\Generic\ProcessExecution {
        Element\Generic\StdoutCollector::callOutputHandler insteadof Element\Generic\ProcessExecution;
    }

    use Element\Generic\StoresEnvironment;
    use Element\Generic\LazyDrain;

    public function __construct(Environment $environment, InfoStream $infoStream)
    {
        $this->environment = $environment;
        $this->infoStream = $infoStream;
    }

    public function fetch() : array
    {
        $this->execute();

        // TODO: Remove Hack
        if (is_file($this->environment->getFilepath('tree.txt'))) {
            $tree = new Data\EnvironmentReferenceData('tree.txt', $this->environment);
        } else {
            $tree = new Data\StringData('drzewko TODO', 'txt');
        }
        // Hack End

        return [
            'regex-text' => new Data\StringData($this->stdout, 'txt'),
            'tree-text' => $tree
        ];
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('explainer', 'regex-source'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('regex-text', 'tree-text'));
    }

    protected function createProcess() : Process
    {
        $compiler = $this->downloadSource('explainer')->getFilepath();

        chmod($compiler, 0555);

        $regex = $this->downloadSource('regex-source')->getString();

        $process = new Process(escapeshellarg($compiler));
        $process->setInput($regex);
        $process->setWorkingDirectory($this->environment->getWorkingDirectory());

        return $process;
    }
}
