<?php
/**
 * Class ClearGccElement
 * @package App\Compiler\Element\Chain
 */


namespace Compiler\Element\Chain;


use Compiler\Data\FileData;
use Compiler\Element\ChainElement;
use Compiler\Element\ClearElement;
use Compiler\Element\GccElement;
use Compiler\Environment;
use Compiler\InfoStream;
use Compiler\Pipe\StaticPipe;

class ExplainerElement extends ChainElement
{
    public function __construct(Environment $environment, InfoStream $infoStream, $include = null)
    {
        parent::__construct($environment, $infoStream);

        $inputLex = $include.DIRECTORY_SEPARATOR.'explainer.l';

        $clear = new ClearElement($environment, $infoStream);
        $gcc = new YaccLexGccElement($environment, $infoStream, 'explainer', $include);

        // interconnections
        $gcc->connect('yacc-source', $this->wrapPipe($clear->pipe('yacc-source')));
        $gcc->connect('lex-source', $this->wrapPipe(new StaticPipe(new FileData($inputLex))));

        $this->addChild('clear', $clear,
            ['clear-source' => 'clear-source', 'clear-compiler' => 'clear-compiler']
        );

        $this->addChild('gcc', $gcc,
            [],
            ['explainer' => 'exec']
        );
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('clear-source', 'clear-compiler'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('explainer'));
    }
}
