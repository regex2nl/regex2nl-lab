<?php
/**
 * Class FullChain
 * @package App\Compiler\Element\Chain
 */


namespace Compiler\Element\Chain;


use Compiler\Element\ChainElement;
use Compiler\Element\RegexElement;
use Compiler\Environment;
use Compiler\InfoStream;

class FullChainElement extends ChainElement
{
    public function __construct(Environment $environment, InfoStream $infoStream, \Closure $env_creator, $include = null)
    {
        parent::__construct($environment, $infoStream);

        // YACC, LEX -> GCC -> Clear compiler
        // Note seperate environment to avoid conflict with ClearGccElement which uses YaccLexGcc as well
        $first = new YaccLexGccElement($env_creator(), $infoStream, 'clear-compiler', $include);
        $second = new ExplainerElement($environment, $infoStream, $include);
        $third = new RegexElement($environment, $infoStream);

        // interconnections
        $second->connect('clear-compiler', $this->chainPipe('clear-compiler', $first->pipe('clear-compiler')));
        $third->connect('explainer', $this->chainPipe('explainer', $second->pipe('explainer')));

        $this->addChild('yacclex', $first,
            ['yacc-source' => 'yacc-source', 'lex-source' => 'lex-source'],
            ['clear-compiler' => 'exec']
        );

        $this->addChild('clear', $second,
            ['clear-source' => 'clear-source'],
            ['explainer' => 'explainer']
        );

        $this->addChild('regex', $third,
            ['regex-source' => 'regex-source'],
            ['regex-text' => 'regex-text', 'tree-text' => 'tree-text']
        );
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, [
            'yacc-source',
            'lex-source',
            'regex-source',

            'clear-compiler',
            'clear-source',
            'explainer'
        ]);
    }

    public function provides(string $name) : bool
    {
        return in_array($name, [
            'clear-compiler',
            'explainer',
            'regex-text',
            'tree-text'
        ]);
    }
}
