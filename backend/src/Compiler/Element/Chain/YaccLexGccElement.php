<?php
/**
 * Class YaccLexGccElement
 * @package App\Compiler\Element\Chain
 */


namespace Compiler\Element\Chain;


use Compiler\Element\ChainElement;
use Compiler\Element\GccElement;
use Compiler\Element\LexElement;
use Compiler\Element\YaccElement;
use Compiler\Environment;
use Compiler\InfoStream;

class YaccLexGccElement extends ChainElement
{
    public function __construct(Environment $environment, InfoStream $infoStream, string $exe, $include = null)
    {
        parent::__construct($environment, $infoStream);

        $yacc = new YaccElement($environment, $infoStream);
        $lex = new LexElement($environment, $infoStream);
        $gcc = new GccElement($environment, $infoStream, $exe, $include);

        // interconnections
        $gcc->connect('source-1', $this->wrapPipe($yacc->pipe('c-source')));
        $gcc->connect('source-2', $this->wrapPipe($lex->pipe('c-source')));

        $this->addChild('lex', $lex,
            ['source' => 'lex-source']
        );

        $this->addChild('yacc', $yacc,
            ['source' => 'yacc-source']
        );

        $this->addChild('gcc', $gcc,
            [],
            ['exec' => 'exec']
        );
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('yacc-source', 'lex-source'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('exec'));
    }
}
