<?php
/**
 * Class LexElement
 * @package App\Compiler\LexElement
 */


namespace Compiler\Element;


use Compiler\Data;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Symfony\Component\Process\Process;

class LexElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;
    use Element\Generic\ProcessExecution;
    use Element\Generic\StoresEnvironment;
    use Element\Generic\LazyDrain;

    public function __construct(Environment $environment, InfoStream $infoStream)
    {
        $this->environment = $environment;
        $this->infoStream = $infoStream;
    }

    public function fetch() : array
    {
        $this->execute();

        $reference = new Data\EnvironmentReferenceData('lex.yy.c', $this->environment);

        $this->infoStream->push(new InfoStream\ArtifactInfo($reference));

        return [
            'c-source' => $reference
        ];
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('source'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('c-source'));
    }

    protected function createProcess() : Process
    {
        $source = $this->downloadSource('source')->getFilepath();

        $process = new Process('/usr/bin/lex '.escapeshellarg($source));
        $process->setWorkingDirectory($this->environment->getWorkingDirectory());

        return $process;
    }
}
