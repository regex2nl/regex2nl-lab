<?php
/**
 * Class ClearElement
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\Data\EnvironmentReferenceData;
use Compiler\Element;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream;
use Symfony\Component\Process\Process;

class ClearElement implements Element, Drainable, UsesEnvironment
{
    use Element\Generic\BasicIoSupport;

    use Element\Generic\StdoutCollector, Element\Generic\ProcessExecution {
        Element\Generic\StdoutCollector::callOutputHandler insteadof Element\Generic\ProcessExecution;
    }

    use Element\Generic\StoresEnvironment;
    use Element\Generic\LazyDrain;

    public function __construct(Environment $environment, InfoStream $infoStream)
    {
        $this->environment = $environment;
        $this->infoStream = $infoStream;
    }

    public function fetch() : array
    {
        $this->execute();

        $name = $this->environment->generateFile('clear-yacc', 'y');
        file_put_contents($this->environment->getFilepath($name), $this->stdout);
        $reference = new EnvironmentReferenceData($name, $this->environment, 'y');

        $this->infoStream->push(new InfoStream\ArtifactInfo($reference));

        return [
            'yacc-source' => $reference
        ];
    }

    public function accepts(string $name) : bool
    {
        return in_array($name, array('clear-compiler', 'clear-source'));
    }

    public function provides(string $name) : bool
    {
        return in_array($name, array('yacc-source'));
    }

    protected function createProcess() : Process
    {
        $compiler = $this->downloadSource('clear-compiler')->getFilepath();

        chmod($compiler, 0555);

        $clear = $this->downloadSource('clear-source')->getString();

        $process = new Process(escapeshellarg($compiler));
        $process->setInput($clear);
        $process->setWorkingDirectory($this->environment->getWorkingDirectory());

        return $process;
    }
}
