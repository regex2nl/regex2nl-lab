<?php
/**
 * Class StoresEnvironment
 * @package App\Compiler\Element\Generic
 */


namespace Compiler\Element\Generic;


use Compiler\Environment;

trait StoresEnvironment
{
    protected $environment = null;

    public function getEnvironment() : Environment
    {
        return $this->environment;
    }
}
