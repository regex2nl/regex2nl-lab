<?php


namespace Compiler\Element\Generic;


use Compiler\CompilerException;
use Compiler\Data;
use Log;

trait LazyDrain
{
    protected $lazyCache = null;

    protected abstract function fetch() : array;

    public function drain(string $name) : Data
    {
        if (is_null($this->lazyCache))
            $this->lazyCache = $this->fetch();

        if (!isset($this->lazyCache[$name]))
            throw new CompilerException("Element is not capable of providing ".$name);

        return $this->lazyCache[$name];
    }
}
