<?php
/**
 * Class BasicIoSupport
 * @package App\Compiler\Element\Generic
 */


namespace Compiler\Element\Generic;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\InfoStream;
use Compiler\Pipe;

trait BasicIoSupport
{
    /**
     * @var InfoStream
     */
    protected $infoStream = null;
    protected $outputHandlers = array();
    protected $sources = array();

    public function connect(string $name, Pipe $source)
    {
        $this->sources[$name] = $source;
    }

    public function pipe(string $name) : Pipe
    {
        return new Pipe\DrainablePipe($this, $name);
    }

    protected function downloadSource($name) : Data
    {
        if (empty($this->sources[$name]))
            throw new CompilerException('Required data not connected: '.$name);

        // $this->callErrorHandler("[".__CLASS__."] Downloading data: ".$name."\n");

        return $this->sources[$name]->drain();
    }

    protected function downloadAllSourcesStartingWith($name) : array
    {
        $matching = array_filter($this->sources, function ($k) use ($name) {
            return substr($k, 0, strlen($name)) == $name;
        }, ARRAY_FILTER_USE_KEY);

        return array_map(function ($v, $k) {
            // $this->callErrorHandler("[".__CLASS__."] Downloading data: ".$k."\n");
            return $v->drain();
        }, $matching, array_keys($matching));
    }
}
