<?php
/**
 * Class ProcessExecution
 * @package App\Compiler\Element\Generic
 */


namespace Compiler\Element\Generic;


use Compiler\CompilerException;
use Log;
use Symfony\Component\Process\Process;
use Compiler\InfoStream;

trait ProcessExecution
{
    protected function callOutputHandler($data)
    {
        $this->infoStream->push(new InfoStream\StdoutInfo($data));
    }

    protected function callErrorHandler($data)
    {
        $this->infoStream->push(new InfoStream\StderrInfo($data));
    }

    abstract protected function createProcess() : Process;

    protected function postExecution($status)
    {
        if ($status != 0)
            throw new CompilerException("Process exited with code: ".$status);
    }

    public function execute()
    {
        // $this->callErrorHandler("[".__CLASS__."] Creating process\n");

        $process = $this->createProcess();

        // $this->callErrorHandler("[".__CLASS__."] Process execution\n");

        $process->start(function ($type, $buffer) {
            if ($type == Process::ERR) {
                $this->callErrorHandler($buffer);
            } else {
                $this->callOutputHandler($buffer);
            }
        });

        $status = $process->wait();

        $this->postExecution($status);
    }
}
