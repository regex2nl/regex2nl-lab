<?php


namespace Compiler\Element\Generic;


trait StdoutCollector
{
    protected $stdout = '';

    protected function callOutputHandler($data)
    {
        $this->stdout .= $data;
    }
}
