<?php
/**
 * Class Drainable
 * @package App\Compiler\Element
 */


namespace Compiler\Element;


use Compiler\Data;

interface Drainable
{
    public function drain(string $name) : Data;
}
