<?php
/**
 * Class EnvironmentWrapperData
 * @package App\Compiler\Data
 */


namespace Compiler\Data;


use Compiler\Data;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;
use Ramsey\Uuid\Uuid;

class EnvironmentWrapperData implements Data, UsesEnvironment
{
    protected $base;
    protected $environment;
    protected $copy;

    public static function wrap(Data $base, Environment $environment) : Data
    {
        if ($base instanceof UsesEnvironment and $base->getEnvironment()->equals($environment))
            return $base;

        return new EnvironmentWrapperData($base, $environment);
    }

    /**
     * EnvironmentWrapperData constructor.
     * @param Data $base
     * @param Environment $environment
     */
    public function __construct(Data $base, Environment $environment)
    {
        $this->base = $base;
        $this->environment = $environment;
    }

    public function getString() : string
    {
        return $this->base->getString();
    }

    public function getFilepath() : string
    {
        if (!empty($this->copy))
            return $this->environment->getFilepath($this->copy);

        $type = $this->getType();
        $this->copy = Uuid::uuid4()->toString().($type ? '.'.$type : '');

        if ($this->base->hasFileBacking()) {
            $this->environment->copy($this->copy, $this->base->getFilepath());
        } else {
            $this->environment->save($this->copy, $this->base->getString());
        }

        return $this->environment->getFilepath($this->copy);
    }

    public function hasFileBacking() : bool
    {
        if (!empty($this->copy))
            return true;

        return $this->base->hasFileBacking();
    }

    public function getEnvironment() : Environment
    {
        return $this->environment;
    }

    public function getType() : string
    {
        return $this->base->getType();
    }
}
