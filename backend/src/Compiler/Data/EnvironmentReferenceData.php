<?php
/**
 * Class EnvironmentReferenceData
 * @package App\Compiler\Data
 */


namespace Compiler\Data;


use Compiler\Data;
use Compiler\Environment;
use Compiler\Environment\UsesEnvironment;

class EnvironmentReferenceData implements Data, UsesEnvironment
{
    protected $environment;
    protected $file;
    protected $type;

    /**
     * EnvironmentReferenceData constructor.
     * @param $file
     * @param Environment $environment
     * @param null $type
     */
    public function __construct($file, Environment $environment, $type = null)
    {
        $this->file = $file;
        $this->environment = $environment;

        if (is_null($type))
            $type = pathinfo($this->file, PATHINFO_EXTENSION);

        $this->type = $type;
    }

    public function getEnvironment() : Environment
    {
        return $this->environment;
    }

    public function getString() : string
    {
        return $this->environment->read($this->file);
    }

    public function getFilepath() : string
    {
        return $this->environment->getFilepath($this->file);
    }

    public function hasFileBacking() : bool
    {
        return true;
    }

    public function getType() : string
    {
        return $this->type;
    }
}
