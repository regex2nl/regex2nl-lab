<?php
/**
 * Class StringData
 * @package App\Compiler\Data
 */


namespace Compiler\Data;


use Compiler\CompilerException;
use Compiler\Data;

class StringData implements Data
{
    protected $data;
    protected $type;

    /**
     * StringData constructor.
     * @param $data
     * @param string $type
     */
    public function __construct($data, $type = 'txt')
    {
        $this->data = $data;
        $this->type = $type;
    }

    public function hasFileBacking() : bool
    {
        return false;
    }

    public function getString() : string
    {
        return $this->data;
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function getFilepath() : string
    {
        // TODO: Implement getFilepath() method.
        throw new CompilerException("getFilepath not implemented");
    }
}
