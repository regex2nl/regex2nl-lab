<?php
/**
 * Class FileData
 * @package App\Compiler\Data
 */


namespace Compiler\Data;


use Compiler\Data;

class FileData implements Data
{
    protected $file;
    protected $type;

    /**
     * FileData constructor.
     * @param $file
     * @param null $type
     */
    public function __construct($file, $type = null)
    {
        $this->file = $file;

        if (is_null($type))
            $type = pathinfo($this->file, PATHINFO_EXTENSION);

        $this->type = $type;
    }

    public function getString() : string
    {
        return file_get_contents($this->file);
    }

    public function getFilepath() : string
    {
        return $this->file;
    }

    public function hasFileBacking() : bool
    {
        return true;
    }

    public function getType() : string
    {
        return $this->type;
    }
}
