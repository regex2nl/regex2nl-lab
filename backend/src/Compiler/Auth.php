<?php

namespace Compiler;


class Auth
{
    protected $password = '';

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function createKey($username)
    {
        $iv = openssl_random_pseudo_bytes(16);

        $hash = sha1($username);

        $encrypted = openssl_encrypt($hash.'|'.$username, 'aes-256-ctr', $this->password, OPENSSL_RAW_DATA, $iv);

        return base64_encode($iv.$encrypted);
    }

    public function verifyKey($key)
    {
        $key = base64_decode($key);

        if (empty($key) or strlen($key) <= 16)
            return false;

        $iv = substr($key, 0, 16);
        $data = substr($key, 16);

        $decrypted = openssl_decrypt($data, 'aes-256-ctr', $this->password, OPENSSL_RAW_DATA, $iv);

        if (empty($decrypted) || strpos($decrypted, '|') === FALSE)
            return false;

        list($hash, $username) = explode('|', $decrypted, 2);

        if (sha1($username) != $hash)
            return false;

        return true;
    }
}
