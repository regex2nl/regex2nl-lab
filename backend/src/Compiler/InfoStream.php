<?php

namespace Compiler;

use Compiler\InfoStream\Info;

class InfoStream
{
    protected $handlers = array();

    public function registerHandler($handler)
    {
        $this->handlers[] = $handler;
    }

    public function push(Info $info)
    {
        foreach ($this->handlers as $h) {
            $h($info);
        }
    }
}
