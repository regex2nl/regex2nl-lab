<?php
/**
 * Class Data
 * @package App\Compiler
 */


namespace Compiler;


interface Data
{
    public function getType() : string;
    public function getString() : string;
    public function getFilepath() : string;
    public function hasFileBacking() : bool;
}
