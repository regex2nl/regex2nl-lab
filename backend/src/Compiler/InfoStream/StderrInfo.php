<?php
/**
 * Created by PhpStorm.
 * User: eplightning
 * Date: 16.01.17
 * Time: 00:57
 */

namespace Compiler\InfoStream;


class StderrInfo implements Info
{
    protected $data;

    public function __construct(string $data)
    {
        $this->data = $data;
    }

    public function isConsoleOutput() : bool
    {
        return true;
    }

    public function __toString()
    {
        return $this->data;
    }

}
