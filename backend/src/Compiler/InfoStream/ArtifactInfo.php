<?php
/**
 * Created by PhpStorm.
 * User: eplightning
 * Date: 16.01.2017
 * Time: 23:12
 */

namespace Compiler\InfoStream;


use Compiler\Data\EnvironmentReferenceData;
use Compiler\Environment;

class ArtifactInfo implements Info
{
    protected $reference;
    protected $binary = false;

    public function __construct(EnvironmentReferenceData $reference, $binary = false)
    {
        $this->reference = $reference;
        $this->binary = $binary;
    }

    /**
     * @return EnvironmentReferenceData
     */
    public function getReference(): EnvironmentReferenceData
    {
        return $this->reference;
    }

    /**
     * @return bool
     */
    public function isBinary(): bool
    {
        return $this->binary;
    }

    public function isConsoleOutput(): bool
    {
        return false;
    }
}
