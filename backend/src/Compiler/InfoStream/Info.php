<?php

namespace Compiler\InfoStream;

interface Info
{
    public function isConsoleOutput() : bool;
}
