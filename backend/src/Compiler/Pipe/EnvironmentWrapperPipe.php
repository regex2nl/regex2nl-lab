<?php


namespace Compiler\Pipe;


use Compiler\Data;
use Compiler\Environment;
use Compiler\Pipe;

class EnvironmentWrapperPipe implements Pipe
{
    protected $environment;
    protected $pipe;

    public function __construct(Pipe $pipe, Environment $environment)
    {
        $this->environment = $environment;
        $this->pipe = $pipe;
    }

    public function drain() : Data
    {
        return Data\EnvironmentWrapperData::wrap($this->pipe->drain(), $this->environment);
    }
}
