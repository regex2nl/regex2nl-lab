<?php
/**
 * Class StaticPipe
 * @package App\Compiler\Pipe
 */


namespace Compiler\Pipe;


use Compiler\Data;
use Compiler\Element;
use Compiler\Pipe;

class StaticPipe implements Pipe
{
    protected $data;

    /**
     * StaticPipe constructor.
     * @param $data
     */
    public function __construct(Data $data)
    {
        $this->data = $data;
    }


    public function drain() : Data
    {
        return $this->data;
    }
}
