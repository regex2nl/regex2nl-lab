<?php


namespace Compiler\Pipe;


use Compiler\CompilerException;
use Compiler\Data;
use Compiler\Element\ChainElement;
use Compiler\Pipe;

class ChainPipe implements Pipe
{
    protected $element;
    protected $source;
    protected $original;

    public function __construct(ChainElement $element, string $source, $original)
    {
        $this->element = $element;
        $this->source = $source;
        $this->original = $original;
    }

    public function drain() : Data
    {
        $data = $this->element->chainDrain($this->source);

        if (is_null($data)) {
            if (is_null($this->original))
                throw new CompilerException("Source not provided for direct connection ".$this->source.", cannot drain");

            $data = $this->original->drain();
        }

        return Data\EnvironmentWrapperData::wrap($data, $this->element->getEnvironment());
    }
}
