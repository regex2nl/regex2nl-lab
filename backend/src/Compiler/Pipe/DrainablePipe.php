<?php
/**
 * Class DrainablePipe
 * @package App\Compiler\Pipe
 */


namespace Compiler\Pipe;


use Compiler\Data;
use Compiler\Element;
use Compiler\Element\Drainable;
use Compiler\Pipe;

class DrainablePipe implements Pipe
{
    protected $element;
    protected $name;

    /**
     * DrainablePipe constructor.
     *
     * @param $element
     * @param $name
     */
    public function __construct(Element $element, string $name)
    {
        $this->element = $element;
        $this->name = $name;
    }

    public function drain() : Data
    {
        return $this->element->drain($this->name);
    }
}
