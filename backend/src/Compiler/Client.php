<?php
/**
 * Created by PhpStorm.
 * User: eplightning
 * Date: 12.12.16
 * Time: 21:29
 */

namespace Compiler;

use React\EventLoop\Factory;
use Ratchet\Client\Connector;
use Throwable;

class Client
{
    /**
     * @var Auth
     */
    protected $auth;
    protected $uri;

    public function __construct($uri, Auth $auth)
    {
        $this->auth = $auth;
        $this->uri = $uri;
    }

    public function compileRegex($expression, $explainer)
    {
        $loop = Factory::create();
        $connector = new Connector($loop);
        $connection = $connector($this->uri, [], []);

        $exception = null;
        $output = '';

        $connection->then(function ($conn) use ($explainer, $expression, &$output, &$exception) {
            $conn->on('message', function ($msg) use ($conn, &$output, &$exception) {
                $msg = json_decode($msg);

                if ($msg->status != 'WORK')
                    $conn->close();

                if ($msg->status == 'FAILURE') {
                    $exception = new CompilerException($msg->console);
                } else if ($msg->status == 'SUCCESS') {
                    $output = $msg->output->regex_text;
                }
            });

            $conn->send(json_encode([
                'type' => 'compile',
                'auth' => $this->auth->createKey('WebServiceAccess'),
                'payload' => [
                    'input' => [
                        'regex_source' => $expression,
                        'explainer' => $explainer
                    ],
                    'output' => ['regex_text']
                ]

            ]));
        }, function ($e) use (&$exception) {
            $exception = $e;
        });

        $loop->run();

        if ($exception != null and $exception instanceof Throwable)
            throw $exception;

        return $output;
    }
}
