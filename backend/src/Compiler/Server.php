<?php
/**
 * Class CompilerServer
 * @package App\Server
 */


namespace Compiler;

use Compiler\Data\EnvironmentReferenceData;
use Compiler\Data\StringData;
use Compiler\Element\Chain\FullChainElement;
use Compiler\Environment\FilesystemEnvironment;
use Compiler\Environment\UsesEnvironment;
use Compiler\InfoStream\ArtifactInfo;
use Compiler\InfoStream\Info;
use Compiler\Pipe\StaticPipe;
use Exception;
use Psr\Log\LoggerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;

class Server implements MessageComponentInterface
{
    /**
     * @var SplObjectStorage
     */
    protected $connections = null;

    /**
     * @var LoggerInterface
     */
    protected $logger = null;

    /**
     * @var string
     */
    protected $storage = '';

    /**
     * @var string
     */
    protected $include = '';

    /**
     * @var \Closure
     */
    protected $keyVerifier = null;

    public function __construct(LoggerInterface $logger, string $storage, string $include, \Closure $verifier)
    {
        $this->connections = new SplObjectStorage;
        $this->logger = $logger;
        $this->storage = $storage;
        $this->include = $include;
        $this->keyVerifier = $verifier;
    }

    public function sendHeartbeat()
    {
        $msg = json_encode(['type' => 'heartbeat', 'payload' => null]);

        foreach ($this->connections as $c)
            $c->send($msg);
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections->attach($conn);

        $this->logger->info('Client connected: '.$this->identify($conn));
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->connections->detach($conn);

        $this->logger->info('Client disconnected '.$this->identify($conn));
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->logger->error('Client error '.$this->identify($conn).': '.$e->getMessage());

        $conn->close();
    }

    private function identify(ConnectionInterface $conn)
    {
        return '['.$conn->remoteAddress.']';
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $msg = json_decode($msg);

        if (is_null($msg)) {
            $this->logger->error('Invalid message from client '.$this->identify($from));
            throw new CompilerException("Invalid message");
        }

        if (empty($msg->type) || !in_array($msg->type, ['compile', 'artifact'])) {
            $this->logger->error('Invalid message type from client '.$this->identify($from));
            throw new CompilerException("Invalid message");
        }

        $verifier = $this->keyVerifier;

        if (empty($msg->auth) || !$verifier($msg->auth)) {
            $this->logger->error('Unauthorized client '.$this->identify($from));
            throw new CompilerException("Invalid message");
        }

        $payload = $msg->payload;

        switch ($msg->type) {
            case 'compile':
                $environment = new FilesystemEnvironment($this->storage);

                try {
                    $this->onMessageDo($from, $payload, $environment);
                } catch (Exception $e) {
                    $environment->dispose();
                    $this->sendFailure($from, $e->getMessage());
                }
                break;

            case 'artifact':
                try {
                    $this->onArtifact($from, $payload);
                } catch (Exception $e) {
                    $this->logger->error('Client failure '.$this->identify($from).': '.$e->getMessage());
                }
                break;
        }
    }

    private function onArtifact(ConnectionInterface $from, $msg)
    {
        if ($msg->type != 'env' || empty($msg->env) || empty($msg->name))
            return;

        $data = new EnvironmentReferenceData(basename($msg->name), new FilesystemEnvironment($this->storage, $msg->env, 0));

        $this->logger->info('Client artifact fetch '.$this->identify($from));

        $from->send(json_encode([
            'type' => 'artifact',
            'payload' => [
                'data' => $data->getString()
            ]
        ]));
    }

    private function onMessageDo(ConnectionInterface $from, $msg, Environment $environment)
    {
        $env_creator = function () {
            return new FilesystemEnvironment($this->storage);
        };

        $artifacts = [];

        $info = new InfoStream();
        $info->registerHandler(function (Info $data) use ($from, &$artifacts) {
            if ($data->isConsoleOutput())
                $this->sendOutput($from, (string) $data);

            if ($data instanceof ArtifactInfo)
                $artifacts[] = [
                    'type' => 'env',
                    'env' => $data->getReference()->getEnvironment()->getName(),
                    'name' => basename($data->getReference()->getFilepath())
                ];
        });

        $chain = new FullChainElement($environment, $info, $env_creator, $this->include);

        $mapping = function ($k)
        {
            return str_replace('_', '-', $k);
        };

        $reverse_mapping = function ($k)
        {
            return str_replace('-', '_', $k);
        };

        $type_mapping = function ($k) {
            switch ($k) {
                case 'yacc-source':
                    return 'y';

                case 'lex-source':
                    return 'l';

                case 'clear-source':
                    return 'clear';
            }

            return 'txt';
        };

        foreach ($msg->input as $k => $v)
        {
            $k = $mapping($k);

            if ($this->endsWith($k, ['source', 'text'])) {
                $data = new StringData($v, $type_mapping($k));
            } else {
                $data = new EnvironmentReferenceData($k, new FilesystemEnvironment($this->storage, $v, 0));
            }

            $chain->connect($k, new StaticPipe($data));
        }

        $output = array();

        foreach ($msg->output as $o) {
            $o = $mapping($o);
            $data = $chain->pipe($o)->drain();

            if ($data instanceof UsesEnvironment && !$this->endsWith($o, ['-text'])) {
                $data = $data->getEnvironment()->getName();
            } else {
                $data = $data->getString();
            }

            $output[$reverse_mapping($o)] = $data;
        }

        $this->logger->info('Client success '.$this->identify($from));

        $from->send(json_encode([
            'type' => 'compile',
            'payload' => [
                'status' => 'SUCCESS',
                'console' => '',
                'output' => $output,
                'artifacts' => $artifacts
            ]
        ]));
    }

    private function endsWith($string, $with)
    {
        foreach ((array) $with as $variant) {
            if (substr($string, -(strlen($variant))) === $variant)
                return true;
        }

        return false;
    }

    private function sendFailure(ConnectionInterface $from, $message)
    {
        $this->logger->error('Client failure '.$this->identify($from).': '.$message);

        $from->send(json_encode([
            'type' => 'compile',
            'payload' => [
                'status' => 'FAILURE',
                'console' => $message,
                'output' => null,
                'artifacts' => null
            ]
        ]));
    }

    private function sendOutput(ConnectionInterface $from, $data)
    {
        $from->send(json_encode([
            'type' => 'compile',
            'payload' => [
                'status' => 'WORK',
                'console' => $data,
                'output' => null,
                'artifacts' => null
            ]
        ]));
    }
}
