<?php
namespace Compiler;

use Exception;

class CompilerException extends Exception {

    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
