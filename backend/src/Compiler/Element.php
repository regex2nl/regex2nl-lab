<?php
/**
 * Class Element
 * @package App\Compiler
 */


namespace Compiler;


interface Element
{
    public function accepts(string $name) : bool;
    public function connect(string $name, Pipe $source);

    public function provides(string $name) : bool;
    public function pipe(string $name) : Pipe;
}
