<?php
/**
 * Class Pipe
 * @package App\Compiler
 */


namespace Compiler;


interface Pipe
{
    public function drain() : Data;
}
