<?php
/**
 * Class Environment
 * @package App\Compiler
 */


namespace Compiler;


interface Environment
{
    public function contains($file);

    public function copy($copy, $getFilepath);

    public function save($copy, $getString);

    public function getFilepath($copy);

    public function generateFile($prefix, $extension);

    public function getName();

    public function read($file);

    public function getWorkingDirectory();

    public function equals(Environment $environment);

    public function dispose();
}
