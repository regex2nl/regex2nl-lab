<?php

namespace AppBundle\Command;


use Compiler\Server;
use Psr\Log\LoggerInterface;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:compile-listen');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $storage = realpath($this->getContainer()->get('kernel')->getRootDir().'/../var/compiler');
        $include = realpath($this->getContainer()->get('kernel')->getRootDir().'/../var/include');
        $auth = $this->getContainer()->get('app.compiler_auth');
        $logger = $this->getContainer()->get('logger');

        $verify = function ($key) use ($auth) {
            return $auth->verifyKey($key);
        };

        $compileServer = new Server($logger, $storage, $include, $verify);

        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $compileServer
                )
            ),
            31410
        );

        $server->loop->addPeriodicTimer(5, function () use ($compileServer) {
            $compileServer->sendHeartbeat();
        });

        $server->run();
    }

}
