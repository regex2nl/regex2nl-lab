<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Clear;
use AppBundle\Entity\Language;
use AppBundle\Entity\Lex;
use AppBundle\Entity\Regex;
use AppBundle\Entity\Yacc;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class FirstVersionsData implements FixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $polish = new Language('pl');
        $polish->setName('Polski');

        $english = new Language('en');
        $english->setName('English');

        $yacc = new Yacc;
        $yacc->setPublished(true);
        $yacc->setSource('Yacc');
        $yacc->setVersion(1);

        $lex = new Lex;
        $lex->setPublished(true);
        $lex->setSource('Lex');
        $lex->setVersion(1);

        $clear = new Clear;
        $clear->setPublished(true);
        $clear->setRepository(Uuid::uuid4()->toString());
        $clear->setClearCompilerRepository(Uuid::uuid4()->toString());
        $clear->setSource('Clear PL');
        $clear->setVersion(1);
        $clear->setLanguage($polish);
        $clear->setLex($lex);
        $clear->setYacc($yacc);

        $clear2 = new Clear;
        $clear2->setPublished(true);
        $clear2->setRepository(Uuid::uuid4()->toString());
        $clear2->setClearCompilerRepository(Uuid::uuid4()->toString());
        $clear2->setSource('Clear EN');
        $clear2->setVersion(1);
        $clear2->setLanguage($english);
        $clear2->setLex($lex);
        $clear2->setYacc($yacc);

        $regex = new Regex;
        $regex->setExpression('/[0-9]+]');
        $regex->setName('Wyrażenie');

        $manager->persist($polish);
        $manager->persist($english);
        $manager->persist($yacc);
        $manager->persist($lex);
        $manager->persist($clear);
        $manager->persist($clear2);
        $manager->persist($regex);

        $manager->flush();
    }
}
