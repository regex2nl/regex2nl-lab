<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Lex;
use Doctrine\ORM\EntityRepository;

class LexRepository extends EntityRepository
{
    public function getLatestVersion()
    {
        return $this->getEntityManager()
                    ->createQuery('select max(l.version) from AppBundle:Lex l')
                    ->getSingleScalarResult();
    }

    /**
     * Unpublish all other lex revisions except passed one
     *
     * @param Lex $lex
     */
    public function unpublishAllExcept(Lex $lex)
    {
        return $this->getEntityManager()
            ->createQuery('update AppBundle:Lex c
                           set c.published = false
                           where c.version = :version and c.id <> :id')
            ->setParameter('version', $lex->getVersion())
            ->setParameter('id', $lex->getId())
            ->execute();
    }
}
