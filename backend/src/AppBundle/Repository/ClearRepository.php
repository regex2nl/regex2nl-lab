<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Clear;
use AppBundle\Entity\Language;
use Doctrine\ORM\EntityRepository;

class ClearRepository extends EntityRepository
{
    public function getLatestVersion(Language $language)
    {
        return $this->getEntityManager()
                    ->createQuery('select max(c.version) from AppBundle:Clear c where c.language = :lang')
                    ->setParameter('lang', $language->getAlpha2())
                    ->getSingleScalarResult();
    }

    /**
     * Get latest clear explainer for specified language
     *
     * @param Language $language
     * @return Clear
     */
    public function getLatestClear(Language $language)
    {
        return $this->getEntityManager()
                    ->createQuery('select c from AppBundle:Clear c 
                                   where c.published = true
                                   and c.language = :lang
                                   order by c.version desc')
                    ->setParameter('lang', $language->getAlpha2())
                    ->setMaxResults(1)
                    ->getSingleResult();
    }

    /**
     * Unpublish all other clear revisions except passed one
     *
     * @param Clear $clear
     */
    public function unpublishAllExcept(Clear $clear)
    {
        return $this->getEntityManager()
                    ->createQuery('update AppBundle:Clear c
                                   set c.published = false
                                   where c.version = :version and c.language = :lang and c.id <> :id')
                    ->setParameter('version', $clear->getVersion())
                    ->setParameter('lang', $clear->getLanguage()->getAlpha2())
                    ->setParameter('id', $clear->getId())
                    ->execute();
    }
}
