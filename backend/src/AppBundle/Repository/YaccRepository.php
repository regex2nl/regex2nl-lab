<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Yacc;
use Doctrine\ORM\EntityRepository;

class YaccRepository extends EntityRepository
{
    public function getLatestVersion()
    {
        return $this->getEntityManager()
                    ->createQuery('select max(y.version) from AppBundle:Yacc y')
                    ->getSingleScalarResult();
    }

    /**
     * Unpublish all other lex revisions except passed one
     *
     * @param Yacc $yacc
     */
    public function unpublishAllExcept(Yacc $yacc)
    {
        return $this->getEntityManager()
            ->createQuery('update AppBundle:Yacc c
                           set c.published = false
                           where c.version = :version and c.id <> :id')
            ->setParameter('version', $yacc->getVersion())
            ->setParameter('id', $yacc->getId())
            ->execute();
    }
}
