<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Yacc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

/**
 * API do Yacc'a
 *
 * @package AppBundle\Controller
 */
class YaccController extends FOSRestController
{
    /**
     * @Get("/api/yacc")
     * @QueryParam(name="published", default="1")
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function listYaccAction(ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Yacc');

        if ($published) {
            $data = $repository->findBy(['published' => true], ['version' => 'desc']);
        } else {
            $data = $repository->findAll();
        }

        return new View($data);
    }

    /**
     * Pobierz szczegóły Yacc'a
     *
     * @Get("/api/yacc/{id}")
     * @QueryParam(name="published", default="1")
     *
     * @ViewAnnotation(serializerGroups={"get"})
     *
     * @param Yacc $yacc
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getYaccAction(Yacc $yacc, ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        if ($published and !$yacc->getPublished()) {
            $yacc = $this->getDoctrine()->getRepository('AppBundle:Yacc')->findOneBy([
                'version' => $yacc->getVersion(),
                'published' => true
            ]);
        }

        return new View($yacc);
    }

    /**
     * Zapisz Yacc'a
     *
     * @Put("/api/yacc/{id}")
     * @RequestParam(name="source", allowBlank=false)
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param Yacc $yacc
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function saveYaccAction(Yacc $yacc, ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');

        $manager = $this->getDoctrine()->getManager();

        $yaccNew = new Yacc;
        $yaccNew->setPublished(true);
        $yaccNew->setSource($source);
        $yaccNew->setVersion($yacc->getVersion());

        $yacc->setPublished(false);

        $manager->persist($yaccNew);
        $manager->flush();

        $manager->getRepository('AppBundle:Yacc')->unpublishAllExcept($yaccNew);

        return new View($yaccNew);
    }

    /**
     * Nowa wersja Yacc'a
     *
     * @Post("/api/yacc")
     * @RequestParam(name="source", allowBlank=false)
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function createYaccAction(ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');

        $manager = $this->getDoctrine()->getManager();

        $version = $this->getDoctrine()->getRepository('AppBundle:Yacc')->getLatestVersion() + 1;

        $yacc = new Yacc;
        $yacc->setPublished(true);
        $yacc->setSource($source);
        $yacc->setVersion($version);

        $manager->persist($yacc);
        $manager->flush();

        return new View($yacc);
    }
}
