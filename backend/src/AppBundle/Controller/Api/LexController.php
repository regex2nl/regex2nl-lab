<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Lex;
use AppBundle\Entity\Yacc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

/**
 * API do Lex'a
 *
 * @package AppBundle\Controller
 */
class LexController extends FOSRestController
{
    /**
     * @Get("/api/lex")
     * @QueryParam(name="published", default="1")
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function listLexAction(ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Lex');

        if ($published) {
            $data = $repository->findBy(['published' => true], ['version' => 'desc']);
        } else {
            $data = $repository->findAll();
        }

        return new View($data);
    }

    /**
     * Pobierz szczegóły Lex'a
     *
     * @Get("/api/lex/{id}")
     * @QueryParam(name="published", default="1")
     *
     * @ViewAnnotation(serializerGroups={"get"})
     *
     * @param Lex $lex
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getLexAction(Lex $lex, ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        if ($published and !$lex->getPublished()) {
            $lex = $this->getDoctrine()->getRepository('AppBundle:Lex')->findOneBy([
                'version' => $lex->getVersion(),
                'published' => true
            ]);
        }

        return new View($lex);
    }

    /**
     * Zapisz Lex'a
     *
     * @Put("/api/lex/{id}")
     * @RequestParam(name="source", allowBlank=false)
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param Lex $yacc
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function saveLexAction(Lex $lex, ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');

        $manager = $this->getDoctrine()->getManager();

        $lexNew = new Lex;
        $lexNew->setPublished(true);
        $lexNew->setSource($source);
        $lexNew->setVersion($lex->getVersion());

        $lex->setPublished(false);

        $manager->persist($lexNew);
        $manager->flush();

        $manager->getRepository('AppBundle:Lex')->unpublishAllExcept($lexNew);

        return new View($lexNew);
    }

    /**
     * Nowa wersja Lex'a
     *
     * @Post("/api/lex")
     * @RequestParam(name="source", allowBlank=false)
     *
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function createLexAction(ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');

        $manager = $this->getDoctrine()->getManager();

        $version = $this->getDoctrine()->getRepository('AppBundle:Lex')->getLatestVersion() + 1;

        $lex = new Lex;
        $lex->setPublished(true);
        $lex->setSource($source);
        $lex->setVersion($version);

        $manager->persist($lex);
        $manager->flush();

        return new View($lex);
    }
}
