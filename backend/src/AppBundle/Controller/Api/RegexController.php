<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Regex;
use AppBundle\Entity\Yacc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * API do wyrażeń
 *
 * @package AppBundle\Controller
 */
class RegexController extends FOSRestController
{
    /**
     * @Get("/api/regex")
     *
     * @return View
     */
    public function listRegexAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Regex');

        $data = $repository->findAll();

        return new View($data);
    }

    /**
     * Aktualizacja regex'a
     *
     * @Post("/api/regex")
     * @RequestParam(name="name", allowBlank=false)
     * @RequestParam(name="expression", allowBlank=false)
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function updateRegexAction(ParamFetcher $paramFetcher)
    {
        $name = $paramFetcher->get('name');
        $expression = $paramFetcher->get('expression');

        $manager = $this->getDoctrine()->getManager();

        $regex = $manager->getRepository('AppBundle:Regex')->findOneBy(['name' => $name]);

        if (!$regex)
            $regex = new Regex;

        $regex->setName($name);
        $regex->setExpression($expression);

        $manager->persist($regex);
        $manager->flush();

        return new View($regex);
    }
}
