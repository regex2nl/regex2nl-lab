<?php
/**
 * Created by PhpStorm.
 * User: eplightning
 * Date: 12.12.16
 * Time: 21:41
 */

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Language;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExplainerController extends FOSRestController
{
    /**
     * @Get("/api/explainer/explain/{language}")
     *
     * @QueryParam(name="expression", allowBlank=false, strict=true)
     * @QueryParam(name="prefer_version", allowBlank=true, requirements="\d+")
     *
     * @param Language $language
     * @param ParamFetcher $paramFetcher
     */
    public function explainSingleAction(Language $language, ParamFetcher $paramFetcher)
    {
        $expression = $paramFetcher->get('expression');
        $version = (int) $paramFetcher->get('prefer_version');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Clear');

        if (!$version) {
            $clear = $repository->getLatestClear($language);
        } else {
            $clear = $repository->findOneBy(['published' => true, 'version' => $version]);
        }

        if (!$clear)
            throw new NotFoundHttpException("Specified version of clear could not be found");

        $compiler = $this->get('app.compiler');

        return ['output' => $compiler->compileRegex($expression, $clear->getRepository())];
    }
}
