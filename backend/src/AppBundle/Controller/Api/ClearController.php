<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Clear;
use AppBundle\Entity\Yacc;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * API do Clear'a
 *
 * @package AppBundle\Controller
 */
class ClearController extends FOSRestController
{
    /**
     * @Get("/api/clear")
     * @QueryParam(name="published", default="1")
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function listClearAction(ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Clear');

        if ($published) {
            $data = $repository->findBy(['published' => true], ['version' => 'desc']);
        } else {
            $data = $repository->findAll();
        }

        return new View($data);
    }

    /**
     * Pobierz szczegóły Clear'a
     *
     * @Get("/api/clear/{id}")
     * @QueryParam(name="published", default="1")
     * @ViewAnnotation(serializerGroups={"get"})
     *
     * @param Clear $clear
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getClearAction(Clear $clear, ParamFetcher $paramFetcher)
    {
        $published = (bool) $paramFetcher->get('published');

        if ($published and !$clear->getPublished()) {
            $clear = $this->getDoctrine()->getRepository('AppBundle:Clear')->findOneBy([
                'version' => $clear->getVersion(),
                'language' => $clear->getLanguage(),
                'published' => true
            ]);
        }

        return new View($clear);
    }

    /**
     * Zapisz Clear'a
     *
     * @Put("/api/clear/{id}")
     * @RequestParam(name="source", allowBlank=false)
     * @RequestParam(name="repository", allowBlank=false, requirements=@Assert\Uuid)
     * @RequestParam(name="clear_compiler_repository", allowBlank=false, requirements=@Assert\Uuid)
     * @RequestParam(name="yacc", allowBlank=false, requirements="\d+")
     * @RequestParam(name="lex", allowBlank=false, requirements="\d+")
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param Clear $clear
     * @param ParamFetcher $paramFetcher
     * @return View
     * @throws EntityNotFoundException
     */
    public function saveClearAction(Clear $clear, ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');
        $repository = $paramFetcher->get('repository');
        $clearRepository = $paramFetcher->get('clear_compiler_repository');

        $manager = $this->getDoctrine()->getManager();

        $yacc = $manager->getRepository('AppBundle:Yacc')->find((int) $paramFetcher->get('yacc'));
        $lex = $manager->getRepository('AppBundle:Lex')->find((int) $paramFetcher->get('lex'));

        if (!$yacc or !$lex)
            throw new EntityNotFoundException();

        $clearNew = new Clear;
        $clearNew->setPublished(true);
        $clearNew->setRepository($repository);
        $clearNew->setClearCompilerRepository($clearRepository);
        $clearNew->setSource($source);
        $clearNew->setVersion($clear->getVersion());
        $clearNew->setLanguage($clear->getLanguage());
        $clearNew->setYacc($yacc);
        $clearNew->setLex($lex);

        $clear->setPublished(false);

        $manager->persist($clearNew);
        $manager->flush();

        $manager->getRepository('AppBundle:Clear')->unpublishAllExcept($clearNew);

        return new View($clearNew);
    }

    /**
     * Nowa wersja Clear'a
     *
     * @Post("/api/clear")
     * @RequestParam(name="source", allowBlank=false)
     * @RequestParam(name="repository", allowBlank=false, requirements=@Assert\Uuid)
     * @RequestParam(name="clear_compiler_repository", allowBlank=false, requirements=@Assert\Uuid)
     * @RequestParam(name="yacc", allowBlank=false, requirements="\d+")
     * @RequestParam(name="lex", allowBlank=false, requirements="\d+")
     * @RequestParam(name="language", allowBlank=false, requirements=@Assert\Language)
     * @ViewAnnotation(serializerGroups={"list"})
     *
     * @param ParamFetcher $paramFetcher
     * @return View
     * @throws EntityNotFoundException
     */
    public function createClearAction(ParamFetcher $paramFetcher)
    {
        $source = $paramFetcher->get('source');
        $repository = $paramFetcher->get('repository');
        $clearRepository = $paramFetcher->get('clear_compiler_repository');

        $manager = $this->getDoctrine()->getManager();

        $yacc = $manager->getRepository('AppBundle:Yacc')->find((int) $paramFetcher->get('yacc'));
        $lex = $manager->getRepository('AppBundle:Lex')->find((int) $paramFetcher->get('lex'));
        $language = $manager->getRepository('AppBundle:Language')->find($paramFetcher->get('language'));

        if (!$yacc or !$lex or !$language)
            throw new NotFoundHttpException("Invalid Lex, Yacc or Language");

        $version = $this->getDoctrine()->getRepository('AppBundle:Clear')->getLatestVersion($language) + 1;

        $clear = new Clear;
        $clear->setPublished(true);
        $clear->setRepository($repository);
        $clear->setClearCompilerRepository($clearRepository);
        $clear->setSource($source);
        $clear->setVersion($version);
        $clear->setLex($lex);
        $clear->setYacc($yacc);
        $clear->setLanguage($language);

        $manager->persist($clear);
        $manager->flush();

        return new View($clear);
    }
}
