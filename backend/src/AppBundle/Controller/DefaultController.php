<?php

namespace AppBundle\Controller;

use Compiler\Auth;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $auth = $this->get('app.compiler_auth');

        $username = 'User';
        $token = $this->get('security.token_storage')->getToken();

        if (!is_null($token))
            $username = $token->getUsername();

        return $this->render('frontend.html.twig', ['key' => $auth->createKey($username)]);
    }
}
