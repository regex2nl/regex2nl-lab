<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Regex
 *
 * @ORM\Table(name="regex")
 * @ORM\Entity
 */
class Regex
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=127, nullable=false)
     * @Groups({"list", "get"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="expression", type="text", nullable=false)
     * @Groups({"list", "get"})
     */
    private $expression;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "get"})
     */
    private $id;



    /**
     * Set name
     *
     * @param string $name
     *
     * @return Regex
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set expression
     *
     * @param string $expression
     *
     * @return Regex
     */
    public function setExpression($expression)
    {
        $this->expression = $expression;

        return $this;
    }

    /**
     * Get expression
     *
     * @return string
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
