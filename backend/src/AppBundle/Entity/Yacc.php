<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Yacc
 *
 * @ORM\Table(name="yacc")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\YaccRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Yacc
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Groups({"get"})
     */
    private $createdAt = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     * @Groups({"list", "get"})
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="text", nullable=false)
     * @Groups({"get"})
     */
    private $source;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean", nullable=false)
     * @Groups({"list", "get"})
     */
    private $published = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "get"})
     */
    private $id;

    /**
     * Set creation date
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($this->createdAt == null)
            $this->createdAt = new \DateTime("now");
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Yacc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Yacc
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Yacc
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Yacc
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
