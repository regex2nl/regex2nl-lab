<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Languages
 *
 * @ORM\Table(name="languages")
 * @ORM\Entity
 */
class Language
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=127, nullable=false)
     * @Groups({"list", "get"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha2", type="string", length=2, options={"fixed": true})
     * @ORM\Id
     * @Groups({"list", "get"})
     */
    private $alpha2;

    /**
     * Language constructor.
     *
     * @param string $alpha2
     */
    public function __construct($alpha2)
    {
        $this->alpha2 = $alpha2;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get alpha2
     *
     * @return string
     */
    public function getAlpha2()
    {
        return $this->alpha2;
    }
}
