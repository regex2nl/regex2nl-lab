<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Clear
 *
 * @ORM\Table(name="clear")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClearRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Clear
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Groups({"get"})
     */
    private $createdAt = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     * @Serializer\Groups({"list", "get"})
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="text", nullable=false)
     * @Serializer\Groups({"get"})
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="repository", type="guid", nullable=false)
     * @Serializer\Groups({"get"})
     */
    private $repository;

    /**
     * @var string
     *
     * @ORM\Column(name="clear_compiler_repository", type="guid", nullable=false)
     * @Serializer\Groups({"get"})
     */
    private $clearCompilerRepository;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean", nullable=false)
     * @Serializer\Groups({"list", "get"})
     */
    private $published = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"list", "get"})
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Language
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_alpha2", referencedColumnName="alpha2", nullable=false)
     * })
     * @Serializer\Groups({"list", "get"})
     */
    private $language;

    /**
     * @var \AppBundle\Entity\Yacc
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Yacc")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="yacc_id", referencedColumnName="id", nullable=false)
     * })
     * @Serializer\Groups({"get"})
     */
    private $yacc;

    /**
     * @var \AppBundle\Entity\Lex
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lex")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lex_id", referencedColumnName="id", nullable=false)
     * })
     * @Serializer\Groups({"get"})
     */
    private $lex;

    /**
     * Set creation date
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($this->createdAt == null)
            $this->createdAt = new \DateTime("now");
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Clear
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Clear
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Clear
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Get clear compiler repository
     *
     * @return string
     */
    public function getClearCompilerRepository()
    {
        return $this->clearCompilerRepository;
    }

    /**
     * Set clear compiler repository
     *
     * @param string $clearCompilerRepository
     *
     * @return Clear
     */
    public function setClearCompilerRepository($clearCompilerRepository)
    {
        $this->clearCompilerRepository = $clearCompilerRepository;

        return $this;
    }

    /**
     * Set repository
     *
     * @param string $repository
     *
     * @return Clear
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Get repository
     *
     * @return string
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Clear
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return Clear
     */
    public function setLanguage(\AppBundle\Entity\Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set yacc
     *
     * @param \AppBundle\Entity\Yacc $yacc
     *
     * @return Clear
     */
    public function setYacc(\AppBundle\Entity\Yacc $yacc)
    {
        $this->yacc = $yacc;

        return $this;
    }

    /**
     * Get yacc
     *
     * @return \AppBundle\Entity\Yacc
     */
    public function getYacc()
    {
        return $this->yacc;
    }

    /**
     * Set lex
     *
     * @param \AppBundle\Entity\Lex $lex
     *
     * @return Clear
     */
    public function setLex(\AppBundle\Entity\Lex $lex)
    {
        $this->lex = $lex;

        return $this;
    }

    /**
     * Get lex
     *
     * @return \AppBundle\Entity\Lex
     */
    public function getLex()
    {
        return $this->lex;
    }
}
